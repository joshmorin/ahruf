use ::harf::{Harf};
use ::ahruf::{Ahruf, Replace};
use ::defs::Attr;
use ::pluming::AhrufLike;
use std::{ops, cmp, convert, fmt, hash, borrow, iter};



/// An owned Ahruf string. 
///
/// This type defines additional functions that do not apply to Ahruf Slices;
/// namely, anything to do with mutating the string's length. This type derefs 
/// to the `Ahruf` type.
///
/// The conversion of unicode strings (strings of types `String` and `&str`) to 
/// Ahruf strings are not nessaraly a 1:1 operation. Refer to 
/// `ahruf::specification::normalizations` for an exact description of how 
/// strings are converted.
#[derive(Clone, PartialEq, Eq)]
pub struct AhrufBuf(Vec<Harf>);

impl AhrufBuf {
    /// Returns a new empty `AhrufBuf` string.
    pub fn new() -> AhrufBuf {
        AhrufBuf(Vec::new())
    }
    
    /// Overwrite `self` with `s`. This function can be used to avoid heap 
    /// allocations if `self` is large enough.
    pub fn reuse_from_str(&mut self, s: &str) {
        use ::harf::CharsHarfsAdaptor;
        self.0.clear();
        self.0.extend(CharsHarfsAdaptor::new(s.chars()));
    }
    
    /// Ahruf version of `reuse_from_str`; Overwrite `self` with `s`. 
    pub fn reuse_from_ahruf(&mut self, s: &Ahruf) {
        self.0.clear();
        /* extend specialization should take care of this
        let required = self.0.capacity() as isize - s.len() as isize;
        if required < 0 {
            self.0.reserve((-required) as usize);
        } */
        self.push_slice(s);
    }
    
    
    /// Returns and removes the last character of Ahruf, if any.
    pub fn pop(&mut self) -> Option<Harf> {self.0.pop()}
    
    /// Appends a Harf to the `AhrufBuf` string.
    pub fn push(&mut self, h: Harf) {self.0.push(h)}
    
    /// Appends an Ahruf slice to Ahruf.
    pub fn push_slice(&mut self, a: &Ahruf) {
        // specialization will most-likely insure memcpy perf
        self.0.extend(a.as_harfs())
    }
    
    /// Insert a Harf at the specified index
    pub fn insert(&mut self, index: usize, harf: Harf) {self.0.insert(index, harf)}
    
    /// Remove the Harf at the specified index
    pub fn remove(&mut self, index: usize) -> Harf {self.0.remove(index)}
    
    /// Only keep any Harf that makes `F` evaluate as `true`, and discard the 
    /// rest.
    ///
    /// Example
    /// -------
    ///
    /// ```
    /// use ahruf::{AhrufBuf, NORM};
    /// 
    /// let mut k = AhrufBuf::from("د ج ا ج ة");
    /// k.retain(|c| c.any(NORM));
    /// assert!(k == "دجاجة");
    /// ```
    pub fn retain<F>(&mut self, f: F) where F: FnMut(&Harf) -> bool {self.0.retain(f)}
    
    /// Clears the contents of the `AhrufBuf` string
    pub fn clear(&mut self) {self.0.clear()}    
    
    /// Removes all Harfs that are classified as AFRAG or IGN.
    ///
    /// See `AhrufBuf::retain` for a more flexable method to remove Harfs.
    pub fn scrub(&mut self) {
        use ::defs::{AFRAG, IGN};
        self.strip(AFRAG|IGN)
    }
    
    /// Strips any Harf with at least one of the specified flags (bitwised)
    ///
    /// See `AhrufBuf::retain` for a more flexable method to remove Harfs.
    pub fn strip(&mut self, bits: Attr) {
        self.retain(|x| !x.any(bits))
    }
    
    /// Replaces all occurences of `needle` within `self` with `replacement`.
    ///
    /// See also: `Ahruf::replace`
    pub fn subsitute<T>(&mut self, needle: T, replacement: &Ahruf) where T: AhrufLike {
        *self = Replace::replace(self, needle, replacement).to_ahruf_buf();
    }
}

#[test]
fn ahruf_buf_test() {
    use ::defs::WS;
    
    let mut cfs = AhrufBuf::from("الدجـــا  ج"); 
    assert!(cfs.len() == 8);
    cfs.strip(WS);
    assert_eq!(cfs.len(), 6);
    assert!(cfs.to_string() == "الدجاج");
}

impl hash::Hash for AhrufBuf {
    fn hash<H>(&self, state: &mut H) where H: hash::Hasher {
        self.as_ref().hash(state)
    }
}

impl borrow::Borrow<Ahruf> for AhrufBuf {
    fn borrow(&self) -> &Ahruf {
        self.as_ref()
    }
}

impl convert::AsRef<Ahruf> for AhrufBuf {
    fn as_ref(&self) -> &Ahruf {
        // self.0.as_ref() 
        <Vec<Harf> as AsRef<[Harf]>>::as_ref(&self.0).as_ref()
    }
}
impl convert::AsMut<Ahruf> for AhrufBuf {
    fn as_mut(&mut self) -> &mut Ahruf {
        self.0.as_mut()
    }
}

impl cmp::PartialEq<Ahruf> for AhrufBuf {
    fn eq(&self, other: &Ahruf) -> bool {
        &self[..] == other
    }
}
impl<'a> cmp::PartialEq<Ahruf> for &'a AhrufBuf {
    fn eq(&self, other: &Ahruf) -> bool {
        &self[..] == other
    }
}
impl<'a> cmp::PartialEq<&'a Ahruf> for AhrufBuf {
    fn eq(&self, other: & &'a Ahruf) -> bool {
        &self[..] == *other
    }
}
impl cmp::PartialEq<str> for AhrufBuf {
    fn eq(&self, other: &str) -> bool {
        self == &AhrufBuf::from(other)
    }
}
impl<'a> cmp::PartialEq<&'a str> for AhrufBuf {
    fn eq(&self, other: &&str) -> bool {
        self == &AhrufBuf::from(*other)
    }
}

impl<'a> convert::From<&'a str> for AhrufBuf {
    fn from(s: &'a str) -> AhrufBuf {
        let mut ahruf = AhrufBuf::from(Vec::with_capacity(s.len()));
        ahruf.reuse_from_str(s);
        ahruf.0.shrink_to_fit();
        ahruf
    }
}
impl convert::From<String> for AhrufBuf {
    fn from(s: String) -> AhrufBuf {
        let s: &str = s.as_ref();
        AhrufBuf::from(s)
    }
}
impl convert::From<Vec<Harf>> for AhrufBuf {
    fn from(v: Vec<Harf>) -> AhrufBuf {
        AhrufBuf(v)
    }
}
impl<'a> convert::From<&'a Ahruf> for AhrufBuf {
    fn from(s: &'a Ahruf) -> AhrufBuf {
        AhrufBuf(s.as_harfs().to_vec())
    }
}
impl<'a> convert::From<Harf> for AhrufBuf {
    fn from(h: Harf) -> AhrufBuf {
        AhrufBuf::from(<Harf as AsRef<Ahruf>>::as_ref(&h))
    }
}
impl<'a> convert::From<char> for AhrufBuf {
    fn from(c: char) -> AhrufBuf {
        use ::flow::Flow;
        c.flow().collect()
    }
}

/* enable once specialization lands
impl<'a, T> convert::From<T> for AhrufBuf where T: Iterator<Item=&'a Ahruf> + 'a {
    fn from(it: T) -> AhrufBuf {
        let mut o = AhrufBuf::new();
        for a in it {
            o.push_all(a)
        }
        o
    }
}
*/


impl fmt::Display for AhrufBuf {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", self.as_ref())
    }
}

impl fmt::Debug for AhrufBuf {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "ab\"{}\"", self)
    }
}

impl ops::Deref for AhrufBuf {
    type Target = Ahruf;
    fn deref(&self) -> &Self::Target {
        self.as_ref()
    }
}

impl ops::DerefMut for AhrufBuf {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.as_mut()
    }
}
impl iter::FromIterator<Harf> for AhrufBuf {
    fn from_iter<T>(iterator: T) -> AhrufBuf where T: IntoIterator<Item=Harf> {
        let v: Vec<_> = iterator.into_iter().collect();
        AhrufBuf::from(v)
    }
}


#[test]
fn replace_inplace_test(){
    let mut a = AhrufBuf::from("صوم");
    a.subsitute('ص', &AhrufBuf::from("نَ"));
    assert!(&a[..] == "نَوم");
    a.subsitute('ن', &AhrufBuf::from("ت"));
    assert!(&a[..] == "نَوم"); // it's not the same character because it has tashkel. Use filters,
}
