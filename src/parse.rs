use ::ahruf::Ahruf;

/// A trait to convert any string-serialized object back to its 
/// native representation.
pub trait FromAhruf {
    type Err;
    fn from_ahruf(s: &Ahruf) -> Result<Self, Self::Err>;
}


fn parse_unsigned_integer(s: &Ahruf) -> Result<u64, ()> {
    use std::u64;
                    
    let mut acc = 0;
    let mut first_run = true;

    for i in s.as_harfs().iter() {
        if let Some(i) = i.to_integer() {
            let i = i as u64;
            
            if !first_run { 
                if acc > u64::MAX/10 { return Err(()) }
                else { acc *= 10 }
            }
            else {
                first_run = false;
            }
            if u64::MAX-acc < i { return Err(()) }
            else { acc += i }
        }
        else {
            return Err(());
        }
    }
    Ok(acc)
}

macro_rules! impl_from_ahruf_for_unsigned {
    ($t: ident) => {
        impl FromAhruf for $t {
            type Err=();
            fn from_ahruf(s: &Ahruf) -> Result<Self, Self::Err> {
                use std::$t;
                let max:u64 = $t::MAX as u64;
                if let Ok(n) = parse_unsigned_integer(s) {
                    if n <= max {
                        return Ok(n as $t);
                    }
                }
                return Err(())
            }
        }
    }
}

macro_rules! impl_from_ahruf_for_signed {
    ($t: ident) => {
        impl FromAhruf for $t {
            type Err=();
            fn from_ahruf(s: &Ahruf) -> Result<Self, Self::Err> {
                use std::$t;
                
                let mut s = s;
                let mut neg = false;
                
                if s[0] == '-' {
                    s = &s[1..];
                    neg = true;
                }
                    
                let max:u64 = $t::MAX as u64;
                
                if let Ok(n) = parse_unsigned_integer(s) {
                    if neg {
                        if n > max + 1 { return Err(()) }
                        if n == 0 { return Ok(0) }
                        
                        return Ok((((n-1) as Self)* -1) - 1)
                    }
                    else {
                        if n > max { return Err(()) }
                        return Ok(n as Self)
                    }
                }
                return Err(())
            }
        }
    }
}

impl_from_ahruf_for_unsigned!(usize);
impl_from_ahruf_for_unsigned!(u64);
impl_from_ahruf_for_unsigned!(u32);
impl_from_ahruf_for_unsigned!(u16);
impl_from_ahruf_for_unsigned!(u8);
impl_from_ahruf_for_signed!(isize);
impl_from_ahruf_for_signed!(i64);
impl_from_ahruf_for_signed!(i32);
impl_from_ahruf_for_signed!(i16);
impl_from_ahruf_for_signed!(i8);


#[test] 
fn test() {
    use ::ahrufbuf::AhrufBuf;
    assert!(AhrufBuf::from("1").parse::<u8>().unwrap_or_else(|_| panic!()) == 1);
    assert!(AhrufBuf::from("0").parse::<u64>().unwrap_or_else(|_| panic!()) == 0);
    assert!(AhrufBuf::from("0").parse::<i64>().unwrap_or_else(|_| panic!()) == 0);
    assert!(AhrufBuf::from("-0").parse::<i64>().unwrap_or_else(|_| panic!()) == 0);
    assert!(AhrufBuf::from("18446744073709551615").parse::<u64>().unwrap_or_else(|_| panic!()) == 18446744073709551615);
    assert!(AhrufBuf::from("9223372036854775807").parse::<i64>().unwrap_or_else(|_| panic!()) == 9223372036854775807);
    assert!(AhrufBuf::from("9223372036854775808").parse::<i64>().is_err());
    assert!(AhrufBuf::from("-9223372036854775808").parse::<i64>().unwrap_or_else(|_| panic!()) == -9223372036854775808);
    assert!(AhrufBuf::from("-9223372036854775809").parse::<i64>().is_err());
    assert!(AhrufBuf::from("100000").parse::<u64>().unwrap_or_else(|_| panic!()) == 100000);
    assert!(AhrufBuf::from("-100000").parse::<u64>().is_err());
    assert!(AhrufBuf::from("-100000").parse::<i64>().unwrap_or_else(|_| panic!()) == -100000);
    assert!(AhrufBuf::from("256").parse::<u8>().is_err());
    assert!(AhrufBuf::from("255").parse::<u8>().unwrap_or_else(|_| panic!()) == 255);
    assert!(AhrufBuf::from("128").parse::<i8>().is_err());
    assert!(AhrufBuf::from("127").parse::<i8>().unwrap_or_else(|_| panic!()) == 127);
    assert!(AhrufBuf::from("128").parse::<i8>().is_err());
    assert!(AhrufBuf::from("-127").parse::<i8>().unwrap_or_else(|_| panic!()) == -127);
    assert!(AhrufBuf::from("-128").parse::<i8>().unwrap_or_else(|_| panic!()) == -128);
    assert!(AhrufBuf::from("-129").parse::<i8>().is_err());
    assert!(AhrufBuf::from("١٤٣٥").parse::<i16>().unwrap() == 1435);
    
}


