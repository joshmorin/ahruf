use ::harf::{Harf, PrivHarf, CharsHarfsAdaptor};
use ::ahruf::{Ahruf};
use ::ahrufbuf::AhrufBuf;
use ::types;
use std::hash;
use std::{convert, cmp, fmt};





pub trait Flow<'a> { 
    type FlowInnerType: Clone + Iterator<Item=Harf> + 'a;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<Self::FlowInnerType>;
    fn flow(&'a self) -> AhrufFlow<Self::FlowInnerType> {self.masked_flow(0)}
}

impl<'a, T> Flow<'a> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> + 'a {
    type FlowInnerType = AhrufFlow<T>;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<AhrufFlow<T>> {
        AhrufFlow::from(self.clone()).set_mask(mask)
    }
}


impl<'a> Flow<'a> for String {
    type FlowInnerType = types::StrFlowIter<'a>;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<types::StrFlowIter<'a>> {
        AhrufFlow::from(&self[..]).set_mask(mask)
    }
}
impl<'a> Flow<'a> for str {
    type FlowInnerType = types::StrFlowIter<'a>;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<types::StrFlowIter<'a>> {
        AhrufFlow::from(self).set_mask(mask)
    }
}
impl<'a> Flow<'a> for &'a str {
    type FlowInnerType = types::StrFlowIter<'a>;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<types::StrFlowIter<'a>> {
        AhrufFlow::from(*self).set_mask(mask)
    }
}
impl<'a> Flow<'a> for AhrufBuf {
    type FlowInnerType = types::AhrufFlowIter<'a>;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<types::AhrufFlowIter<'a>> {
        AhrufFlow::from(types::ahruf_flow_iter(self.as_harfs().iter().cloned())).set_mask(mask)
    }
}
impl<'a> Flow<'a> for Ahruf {
    type FlowInnerType = types::AhrufFlowIter<'a>;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<types::AhrufFlowIter<'a>> {
        AhrufFlow::from(types::ahruf_flow_iter(self.as_harfs().iter().cloned())).set_mask(mask)
    }
}

impl<'a> Flow<'a> for &'a Ahruf {
    type FlowInnerType = types::AhrufFlowIter<'a>;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<types::AhrufFlowIter<'a>> {
        AhrufFlow::from(types::ahruf_flow_iter(self.as_harfs().iter().cloned())).set_mask(mask)
    }
}
impl<'a> Flow<'a> for Harf {
    type FlowInnerType = types::HarfFlowIter;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<types::HarfFlowIter> {
        AhrufFlow::from(*self).set_mask(mask)
    }
}
impl<'a> Flow<'a> for char {
    type FlowInnerType = types::CharFlowIter;
    fn masked_flow(&'a self, mask: u32) -> AhrufFlow<types::CharFlowIter> {
        AhrufFlow::from(*self).set_mask(mask)
    }
}



/// A iterator-based Ahruf type.
///
/// Objects of this type do not store Harfs but will process the required
/// information from a source on demand. This type is designed for use-cases where 
/// the combined cost of computation from source and preforming the desired operation is 
/// lower than the cost of allocating memory, preforming the operation, and copying
/// the result; i.e. One-off operations or operations that cause significant
/// restructuring of the underlying memory buffer.
///
/// No heap allocations occur when generating an AhrufFlow instance. A heap 
/// allocation may occur for in some of the iterator's methods.
///
/// This type also behaves as the main Harf iterator type for both `Ahruf` 
/// slices and `AhrufBuf` objects.
///
/// ##Filtering and Masks##
/// 
/// This type provides the primary method to preform Normalization in the Ahruf
/// library. Well-defined normalizations are preformed by passing "masks" that 
/// imply a set of strictly-defined conversions and filters to the `mask` 
/// function defined by the `Flow` trait.
///
/// Multiple Masks can be selected simultaneously using the bitwise-or operator.
///
/// 1. **`TASH`**: Strips Tashkel.
/// 2. **`NUM`**: Converts numerals as their U+66X equivalents.
/// 3. **`CALEF`**: Normalizes all characters classified as `CALEF` to 'ا'.
/// 4. **`CTEH`**: Normalizes all characters classified as `CTEH` to 'ت'.
/// 5. **`CHAMZA`**: Normalizes all characters classified as `CHAMZA` to 'ء'.
/// 6. **`0`**: No masking.
/// 7. *Passing any value other than the values specified above is undefined.*
/// 
/// (See the documentation in the specification module for a more ridged 
/// specification of what behavior a mask implies)
///
/// When normalization is preformed, Harfs go through a set of stages as they
/// pass through the normalization pipeline before being returned. These stages are ordered.
/// The flowing list are masks ordered according to the order in which stages are 
/// applied:
/// 
/// 1. `NUM`
/// 2. `CALEF`
/// 3. `CHAMZA`
/// 4. `CTEH`
/// 5. `TASH`
/// 
/// Note that this ordering is part of the API once the library is stabilized.
///
/// Alternate ordering can be achieved by explicitly staging them if 
/// an alternate normalization is desired.
///
/// ```
///     use ahruf::{Flow, CALEF, CHAMZA};
///
///     // Implicit ordering
///     assert!("إلى".masked_flow(CHAMZA|CALEF) == "الا");
///
///     // Explicit ordering
///     assert!("إلى".masked_flow(CALEF).masked_flow(CHAMZA) == "الا");
///     assert!("إلى".masked_flow(CHAMZA).masked_flow(CALEF) == "ءلا");
///
/// ```
///
/// ##Examples##
/// 
/// ```
///     use ahruf::{AhrufBuf, Flow, NUM, TASH, CALEF};
///
///     // Converts
///     assert!("123".masked_flow(NUM) == "١٢٣".masked_flow(NUM)); 
///
///     // Converts Alef forms into a single form
///     assert!("إفعل".masked_flow(CALEF) == "افعل");
///     
///     // An AhrufFlow can be converted into an AhrufBuf
///     let source: AhrufBuf = "الدَجاجةُ لا يَبيضُ.".masked_flow(TASH).collect();
///
///     // .. and back
///     assert!(source.flow().contains("دجاجة".flow()) == true);
///     
///     /*
///     let needle = AhrufBuf::from("دجاجة"); // Notice the lack of taskel
///     let replacement = AhrufBuf::from("ديكُ"); 
///     assert_eq!(source.replace(TASH, needle, replacement), "الديكُ لا يَبيضُ.");*/
///
/// ```
///
/// ##Performance##
///
/// - Computation-wise, `AhrufFlow` is iterator-based, and will behave like an 
///   iterator. How performant external iterators behave is dependent on how 
///   well the compiler optimizes the resulting code.
/// 
///
/// ##Note##
/// 
/// 1. When equating an Ahruf slice to an object that that does not have a 
/// compatible in-memory representation (e.g. a slice of Harf elements to a 
/// utf-8 slice of bytes), both sides are automatically converted to AhrufFlow 
/// objects.
///
/// 2. `AhrufFlow` is clonable, and clones will be in the same state as the 
/// original at the time of the clone.


#[derive(Clone)]
pub struct AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    iter: T,
    mask: u32,
}

impl<T> AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    pub fn set_mask(self, mask: u32) -> AhrufFlow<T> {
        let mut s = self;
        s.mask = mask;
        s
    }
}

impl<T> convert::From<T> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn from(i: T) -> AhrufFlow<T> {
        AhrufFlow{iter: i, mask: 0}
    }
}
/* enable when specialization is good enough to allow this
impl<'a, T> convert::From<T> for AhrufFlow<T> where T: Clone + Iterator<Item=&'a Ahruf> +'a {
    fn from(i: T) -> AhrufFlow<AhrufsHarfsAdaptor<'a, T>> {
        AhrufFlow{iter: AhrufsHarfsAdaptor::from(i), mask: 0}
    }
}*/
impl<'a> convert::From<&'a str> for AhrufFlow<types::StrFlowIter<'a>> {
    fn from(s: &'a str) -> AhrufFlow<types::StrFlowIter<'a>> {
        AhrufFlow{ iter:types::str_flow_iter(CharsHarfsAdaptor::new(s.chars())), mask: 0 }
    }
}
impl convert::From<char> for AhrufFlow<types::CharFlowIter> {
    fn from(c: char) -> AhrufFlow<types::CharFlowIter> {
        AhrufFlow{ iter:types::char_flow_iter(CharsHarfsAdaptor::new(Some(c).into_iter())), mask: 0 }
    }
}
impl<'a> convert::From<&'a char> for AhrufFlow<types::CharFlowIter> {
    fn from(c: &'a char) -> AhrufFlow<types::CharFlowIter> {
        AhrufFlow::from(*c)
    }
}
impl convert::From<Harf> for AhrufFlow<types::HarfFlowIter> {
    fn from(c: Harf) -> AhrufFlow<types::HarfFlowIter> {
        AhrufFlow{ iter:types::harf_flow_iter(Some(c).into_iter()), mask: 0 }
    }
}



impl<T> Iterator for AhrufFlow<T> where T: Clone + Iterator<Item=Harf>{
    type Item = Harf;
    fn next(&mut self) -> Option<Self::Item> {
        use ::defs::{TASH, NUM, AFRAG, CALEF, CHAMZA, CTEH, IGN};
        use std::char;
        
        loop {
            let mut h = if let Some(h) = self.iter.next() {h} else {return None};
            
            if h.any(AFRAG|IGN) {continue}
            
            if (self.mask&NUM != 0) && h.any(NUM) {
                h = PrivHarf::from_char(char::from_u32(h.to_integer().expect("not an integer")+0x660).expect("definitely a bug! id:1"));
            }
            
            if (self.mask&CALEF != 0) && h.any(CALEF) {
                let mut alef = PrivHarf::from_char('ا'); //PREF: static, compile-time for slight boost
                alef.copy_tashkel(h);
                h = alef;
            }
            
            if (self.mask&CHAMZA != 0) && h.any(CHAMZA) {
                let mut r = PrivHarf::from_char('ء'); //PREF: static, compile-time for slight boost
                r.copy_tashkel(h);
                h = r;
            }
            
            if (self.mask&CTEH != 0) && h.any(CTEH) {
                let mut r = PrivHarf::from_char('ت'); //PREF: static, compile-time for slight boost
                r.copy_tashkel(h);
                h = r;
            }
                
            if self.mask&TASH != 0 { 
                h.strip_tashkel();
            }
            
            return Some(h);
        }
    }
}

impl<T> hash::Hash for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn hash<H>(&self, state: &mut H) where H: hash::Hasher {
        for h in self.clone() { h.hash(state); }
    }
}

impl<T> AhrufFlow<T> where  T: Clone + Iterator<Item=Harf>  {
    /// finds the index of the sub-sequence `needle` and returns its offset, 
    /// if any.
    pub fn index_of<U>(&self, needle: AhrufFlow<U>) -> Option<usize> where U: Clone + Iterator<Item=Harf> {
        let mut haystack = self.clone();
        let mut offset: usize = 0;
        
        loop {
            let mut so = haystack.clone();
            let mut nc = needle.clone();
            loop {
                let s = so.next();
                let o = nc.next();
                
                if o.is_none() {return Some(offset)}
                if s.is_none() || !(s.unwrap() == o.unwrap()) {break}
            }
            
            if haystack.next().is_none() {return None;}
            else {offset+=1}
        }
        
    }
    
    /// Checks if self contains the sub-sequence `needle`
    pub fn contains<U>(&self, needle: AhrufFlow<U>) -> bool where U: Clone + Iterator<Item=Harf> {
        self.index_of(needle).is_some()
    }
    
    
    /// Consumes an AhrufFlow and itself, and checks if they are equivilant.
    pub fn ceq<U>(self, needle: AhrufFlow<U>) -> bool where U: Clone + Iterator<Item=Harf> {
        let mut sr = self;
        let mut or = needle;
        loop {
            let s = sr.next();
            let o = or.next();
            
            // if one has a value and the other doesn't
            if s.is_some() == o.is_none() {return false}
            // if they are the same and both are none
            else if o.is_none() {return true}
            
            if !(s.unwrap() == o.unwrap()) {return false}
        }
    }
    
    
    pub fn replace<'a, 'b, U, V>(&self, needle: &'a U, replacement: &'b V) -> AhrufFlow<ReplaceFlow<T, <U as Flow<'a>>::FlowInnerType, <V as Flow<'b>>::FlowInnerType>> where U: Flow<'a>, V: Flow<'b> {
        AhrufFlow::from(ReplaceFlow::replace(self.clone(), needle.flow(), replacement.flow()))
    }
}

impl<T, U> cmp::PartialEq<AhrufFlow<U>> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf>,
                                                                  U: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &AhrufFlow<U>) -> bool {
        self.clone().ceq(other.clone())
    }
}
impl<T> cmp::Eq for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {}

impl<T> cmp::PartialEq<Ahruf> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &Ahruf) -> bool {
        self.clone().ceq(other.flow())
    }
}
impl<'a, T> cmp::PartialEq<Ahruf> for &'a AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &Ahruf) -> bool {
        (*self).clone().ceq(other.flow())  
    }
}


#[derive(Clone, PartialEq, Debug)]
enum State {
    Consume(Option<usize>), // If None, this means consume to end
    Search,
    Replace
}
#[derive(Clone)]
pub struct ReplaceFlow<T, U, V> where T: Clone + Iterator<Item=Harf>,
                                      U: Clone + Iterator<Item=Harf>,
                                      V: Clone + Iterator<Item=Harf> {
    haystack: AhrufFlow<T>,
    target: AhrufFlow<U>,
    target_length: usize,
    replacement: AhrufFlow<V>,
    replacement_current: Option<AhrufFlow<V>>, // In progress replacement
    state: State,
}
impl<T, U, V> ReplaceFlow<T, U, V> where T: Clone + Iterator<Item=Harf>, 
                                U: Clone + Iterator<Item=Harf>,
                                V: Clone + Iterator<Item=Harf> {
    pub fn replace(haystack: AhrufFlow<T>, needle: AhrufFlow<U>, replacement: AhrufFlow<V>) -> ReplaceFlow<T, U, V> {
        ReplaceFlow {
            haystack: haystack,
            replacement: replacement,
            replacement_current: None,
            target: needle.clone(),
            target_length: needle.count(),
            state: State::Search
        }
    }
}
impl<T, U, V>  Iterator for ReplaceFlow<T, U, V> where T: Clone + Iterator<Item=Harf>, 
                                                       U: Clone + Iterator<Item=Harf>,
                                                       V: Clone + Iterator<Item=Harf> {
    type Item = Harf;
    fn next(&mut self) -> Option<Harf> {
        loop {
            /*
            println!("current state is {:?}", self.state);
            println!("current haystack is '{}'", self.haystack);*/
            if self.state == State::Search {
                self.state = if self.target_length == 0 {State::Consume(None)}
                             else                       {State::Consume(self.haystack.index_of(self.target.clone()))}
            }
            
            if let State::Consume(Some(count)) = self.state {
                
                if count != 0 {
                    self.state = State::Consume(Some(count-1));
                    return self.haystack.next();
                }
                else {
                    self.state = State::Replace;
                    self.replacement_current = Some(self.replacement.clone());
                }
            }
            else if State::Consume(None) == self.state {
                return self.haystack.next();
            }
            
            if self.state == State::Replace {
                let r = self.replacement_current.as_mut().unwrap().next();
                if r.is_some() {
                    return r;
                }
                else {
                    self.state = State::Search;
                    for _ in 0..self.target_length {self.haystack.next();} // Bleeding what's being replaced
                    continue;
                }
            }
            unreachable!();
        }
    }
}
#[test]
fn test_flow_replace() {
    assert!(AhrufFlow::from(ReplaceFlow::replace("My hoby is socer.".flow(), "socer".flow(), "football".flow())) == "My hoby is football.");
    assert!(AhrufFlow::from(ReplaceFlow::replace("".flow(), "socer".flow(), "football".flow())) == "");
    assert!(AhrufFlow::from(ReplaceFlow::replace("aaa".flow(), "aaa".flow(), "b".flow())) == "b");
    assert_eq!(AhrufFlow::from(ReplaceFlow::replace("aaa".flow(), "".flow(), "b".flow())), "aaa");
    assert_eq!(AhrufFlow::from(ReplaceFlow::replace("baaab".flow(), "a".flow(), "".flow())), "bb");
    assert_eq!(AhrufFlow::from(ReplaceFlow::replace("aaa".flow(), "aaa".flow(), "".flow())), "");
}

macro_rules! impl_partial_eq_for_ty_ref {
    ($t:ty) => {
        impl<'a, T> cmp::PartialEq<AhrufFlow<T>> for &'a $t where T: Clone + Iterator<Item=Harf> {
            fn eq(&self, other: &AhrufFlow<T>) -> bool {
                use ::flow_replacement::Flow_;
                self.to_ahruf_flow().ceq(other.clone())
            }
        }
        impl<'a, T> cmp::PartialEq<&'a $t> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
            fn eq(&self, other: &&'a $t) -> bool {
                *other == *self
            }
        }
    };
}
macro_rules! impl_partial_eq_for_ty {
    ($t:ty) => {
        impl<T> cmp::PartialEq<AhrufFlow<T>> for $t where T: Clone + Iterator<Item=Harf> {
            fn eq(&self, other: &AhrufFlow<T>) -> bool {
                use ::flow_replacement::Flow_;
                self.to_ahruf_flow().ceq(other.clone())
            }
        }
        impl<T> cmp::PartialEq<$t> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
            fn eq(&self, other: &$t) -> bool {
                *other == *self
            }
        }
    };
}

impl<'a, T> cmp::PartialEq<AhrufFlow<T>> for &'a str where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &AhrufFlow<T>) -> bool {
        AhrufFlow::from(*self).ceq(other.clone())
    }
}
impl<'a, T> cmp::PartialEq<&'a str> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &&'a str) -> bool {
        *other == *self
    }
}
impl<'a, T> cmp::PartialEq<AhrufFlow<T>> for &'a Ahruf where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &AhrufFlow<T>) -> bool {
        self.flow().ceq(other.clone())
    }
}
impl<'a, T> cmp::PartialEq<&'a Ahruf> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &&'a Ahruf) -> bool {
        *other == *self
    }
}
impl<T> cmp::PartialEq<AhrufFlow<T>> for AhrufBuf where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &AhrufFlow<T>) -> bool {
        self.flow().ceq(other.clone())
    }
}
impl<T> cmp::PartialEq<AhrufBuf> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &AhrufBuf) -> bool {
        *other == *self
    }
}
impl<T> cmp::PartialEq<AhrufFlow<T>> for Harf where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &AhrufFlow<T>) -> bool {
        self.flow().ceq(other.clone())
    }
}
impl<T> cmp::PartialEq<Harf> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &Harf) -> bool {
        *other == *self
    }
}
impl<T> cmp::PartialEq<AhrufFlow<T>> for char where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &AhrufFlow<T>) -> bool {
        AhrufFlow::from(*self).ceq(other.clone())
    }
}
impl<T> cmp::PartialEq<char> for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn eq(&self, other: &char) -> bool {
        *other == *self
    }
}


impl<T> fmt::Display for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let mut out = String::new();
        // stupid, but dealing with finicky rust is not a priority 
        for c in self.clone() {
            for i in c.to_string().chars() {
                out.push(i);
            }
        }
        write!(f, "{}", out)
    }
}

impl<T> fmt::Debug for AhrufFlow<T> where T: Clone + Iterator<Item=Harf> {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "af\"{}\"", self)
    }
}


#[test]
fn masked_tests() {
    use ::defs::{NUM, CALEF, CTEH, CHAMZA};
    use self::Flow;
    assert!("129".masked_flow(NUM) == "١٢٩");
    assert!("١٢٩" == "129".masked_flow(NUM));
    assert!("129".masked_flow(NUM) == AhrufBuf::from("١٢٩"));
    assert!("افعل".masked_flow(CALEF) == "إفعل".masked_flow(CALEF));
    assert!("جحا".masked_flow(CALEF) == "جحى".masked_flow(CALEF));
    assert!(PrivHarf::from_char('أ').masked_flow(CALEF) == 'ا');
    
    assert!("مدرسة".masked_flow(CTEH) == "مدرست".masked_flow(CTEH));
    
    //let m = ;
    
    assert!("لدي 11 أرنب".masked_flow(CALEF).masked_flow(NUM) == "لدي 11 أرنب".masked_flow(CALEF|NUM));
    
    assert!("إلى".masked_flow(CHAMZA).masked_flow(CALEF) == "ءلا");
    assert!("إلى".masked_flow(CALEF).masked_flow(CHAMZA) == "الا");
    assert!("إلى".masked_flow(CALEF).masked_flow(CHAMZA) != "ءلا");
    assert!("إلى".masked_flow(CHAMZA).masked_flow(CALEF) != "الا");
}



#[test]
fn map_test() {
    use std::collections::HashMap;
    use ::defs::NUM;
    use self::Flow;
    
    let x = "12";
    let mut map: HashMap<AhrufFlow<_>, usize> = HashMap::new();
    map.insert(x.masked_flow(NUM), 3);
    assert!(*map.get(&"١٢".masked_flow(NUM)).unwrap() == 3);
}


#[test] fn lala() {
    use ::defs::TASH;
    use self::Flow;
    
    let lol = AhrufBuf::from("abc");
    assert!(lol.flow().count() == 3);
    assert!(lol.flow().count() == 3);
    assert!((&lol).flow().count() == 3);
    assert!("cdef".flow().count() == 4);
    //assert!('س'.flow().count() == 1);
    assert!({let r:AhrufBuf = "سُبُلٌ".masked_flow(0).collect(); r} != "سبل");
    assert!({let r:AhrufBuf = "سُبُلٌ".masked_flow(TASH).collect(); r} == "سبل");
    assert!("سُبُلٌ".masked_flow(TASH) == "سبل");
    //assert!("s" == 's'.flow());
    
    
}
