use ::harf::*;
use ::ahrufbuf::AhrufBuf;
use ::defs::Attr;
use ::parse::FromAhruf;
use ::flow::Flow;
use ::pluming::AhrufLike;

use std::{ops, cmp, slice, convert, fmt, iter};

static EMPTY_HARFS: &'static [Harf] = &[];



pub struct AhrufsHarfsAdaptor<'a, T> where T: Iterator<Item=&'a Ahruf> + 'a {
    chunk_iter: T,
    slice_iter: Option<slice::Iter<'a, Harf>>
}

impl<'a, T> convert::From<T> for AhrufsHarfsAdaptor<'a, T> where T: Iterator<Item=&'a Ahruf> + 'a {
    fn from(slices: T) -> AhrufsHarfsAdaptor<'a, T> {
        AhrufsHarfsAdaptor {
            chunk_iter: slices,
            slice_iter: None
        }
    }
}
impl<'a, T> Iterator for AhrufsHarfsAdaptor<'a, T> where T: Iterator<Item=&'a Ahruf> + 'a {
    type Item = Harf;
    fn next(&mut self) -> Option<Harf> {
        loop { 
            if self.slice_iter.is_none() {
                self.slice_iter = self.chunk_iter.next().map(|x| x.as_harfs().iter());
            }
            if self.slice_iter.is_none() {
                return None;
            }
            let next = self.slice_iter.as_mut().unwrap().next().map(|h| *h);
            if next.is_none() {
                self.slice_iter = None;
                continue;
            }
            else {
                return next;
            }
        }
    }
}




/// An iterator over slices returned by `Ahruf`'s split functions.
pub struct Split<'a, F> where F: FnMut(&Harf) -> bool {iter:slice::SplitN<'a, Harf, F>}

impl<'a, F> iter::Iterator for Split<'a, F> where F: FnMut(&Harf) -> bool {
    type Item = &'a Ahruf;
    fn next(&mut self) -> Option<Self::Item> {
        self.iter.next().map(|x| harfs_as_ahruf(x))
    }
}







pub struct Hunks<'a, F, C> where F: FnMut(&Harf) -> Option<C> + 'a,
                                     C: PartialEq + Copy {
    classifier  :F,
    sr          :&'a Ahruf,
    cur         :usize
}
impl<'a, F, C> iter::Iterator for Hunks<'a, F, C> where F: FnMut(&Harf) -> Option<C> + 'a,
                                                      C: PartialEq + Copy {

    type Item = (C, &'a Ahruf);
    
    fn next(&mut self) -> Option<Self::Item> {
        let mut last: Option<(C, usize)> = None;
        let classifier = &mut self.classifier;
        
        for i in self.sr[self.cur..].as_harfs().iter().enumerate() {
            let slot = classifier(i.1);
            
            if let Some(&(last_slot, last_marker)) = last.as_ref() {
                match slot {
                    Option::None => {
                        last = None;
                    },
                    Option::Some(current_slot) => {
                        if current_slot != last_slot {
                            let o = &self.sr[self.cur+last_marker..self.cur+i.0];
                            self.cur += i.0;
                            return Some((last_slot, o));
                        }
                    }
                }
            }
            else {
                if slot.is_some() {
                    last = Some((slot.unwrap(), i.0));
                }
            }
        }
        
        
        if last.is_some() {
            let (last_slot, last_marker) = last.unwrap();
            let o = &self.sr[self.cur+last_marker..];
            self.cur = self.sr.len();
            Some((last_slot, o))
        }
        else {
            self.cur = self.sr.len();
            None
        }
    }
}





pub struct Replace<'a, T> where T: AhrufLike {
    slice: &'a Ahruf,               // Slice to operate on
    needle: T,                      // Needle to search for
    needle_len: usize,              // Length of needle (length computation may be expensive)
    replacement: &'a Ahruf,         // What string is used to replace `needle`
    cur: usize,                     // Current scanning cursor location
    next_return: Option<&'a Ahruf>  // This value should be returned on the next invokation of `next()`
}
impl<'a, T> Replace<'a, T> where T: AhrufLike {
    pub fn replace(slice: &'a Ahruf, needle: T, replacement: &'a Ahruf) -> Replace<'a, T> {
        let l = needle.ahruf_count();
        Replace {
            slice: slice,
            needle: needle,
            needle_len: l,
            replacement: replacement,
            cur: 0,
            next_return: None
        }
    }
    pub fn to_ahruf_buf(self) -> AhrufBuf {
        AhrufsHarfsAdaptor::from(self).collect()
    }
}
impl<'a, T> Iterator for Replace<'a, T> where T: AhrufLike {
    type Item = &'a Ahruf;
    fn next(&mut self) -> Option<Self::Item> {
        if self.next_return.is_some() {
            use std::mem;
            mem::replace(&mut self.next_return, None)
        }
        else {
            let new_offset = if self.needle_len == 0 {None}
                             else {self.slice[self.cur..].index_of(&self.needle)};
            
            if let Some(m) = new_offset {
                if m == 0 {
                    self.cur += self.needle_len;
                    Some(self.replacement)
                }
                else {
                    let r = &self.slice[self.cur..self.cur+m];
                    self.next_return = Some(self.replacement);
                    self.cur += m + self.needle_len;
                    Some(r)                
                }
            }
            else {
                let tail = &self.slice[self.cur..];
                if tail.len() != 0 {
                    self.cur = self.slice.len();
                    Some(tail)
                }
                else {
                    None
                }
            }
        }
    }
}

#[test]
fn test_replace_iter() {
    let r = AhrufBuf::from("z");
    let s = AhrufBuf::from("abe is a chicken baby");
    let s: AhrufBuf = AhrufsHarfsAdaptor::from(Replace::replace(&s, AhrufBuf::from("a"), &r)).collect();
    assert_eq!(s, "zbe is z chicken bzby");
}






/// An Ahruf slice (Akin to `str` and `Path`).
///
/// This type can be used to reffer to an Ahruf slice from either a runtime 
/// buffer (`AhrufBuf`), or static buffer set at compile-time.
///
/// Like all slices, this type is unsized, and can only be used behind a 
/// pointer.
#[derive(Eq, Hash)]
pub struct Ahruf([Harf]);


/// Converts a `[Harf]` slice to an Ahruf slice.
pub fn harfs_as_ahruf(s: &[Harf]) -> &Ahruf {
    use std::mem;
    unsafe {mem::transmute(s)}
}

/// Converts a mutable `[Harf]` slice to a mutable Ahruf slice.
pub fn harfs_as_mut_ahruf(s: &mut [Harf]) -> &mut Ahruf {
    use std::mem;
    unsafe {mem::transmute(s)}
}

impl Ahruf {
    /// Converts a Ahruf slice to a [Harf] slice.
    pub fn as_harfs(&self) -> &[Harf] {
        use std::mem;
        unsafe {mem::transmute(self)}
    }
    
    /// Returns a mutable [Harf] slice.
    pub fn as_harfs_mut(&mut self) -> &mut [Harf] {
        use std::mem;
        unsafe {mem::transmute(self)}
    }
    
    /// Returns a new AhrufBuf object
    pub fn to_owned(&self) -> AhrufBuf {
        AhrufBuf::from(self)
    }
    
    /// Returns the number of `Harf` elements that comprise the Ahruf slice.
    ///
    /// ```
    ///     use ahruf::{AhrufBuf};
    /// 
    ///     assert!(AhrufBuf::from("دجاج").len() == 4);
    /// ```
    pub fn len(&self) -> usize                                                  {self.as_harfs().len()}

    /// Splits the string at any element matching `predicate`. For all elements 
    /// that `predicate` returns `true` will not be inculded in the result, and 
    /// at which a split will occur. Empty strings from consecutive splits are 
    /// not removed.
    ///
    /// 
    /// ```
    ///     use ahruf::{AhrufBuf, Ahruf};
    ///
    ///     let primary = AhrufBuf::from("بيضة\nدجاجة\n\nبيضة");
    ///     let si: Vec<&Ahruf> = primary.split(|h| h.base() == '\n')
    ///                                  .collect();
    ///
    ///     assert!(si[0] == "بيضة");
    ///     assert!(si[1] == "دجاجة");
    ///     assert!(si[2] == "");
    ///     assert!(si[3] == "بيضة");
    ///     assert!(si.len() == 4);
    /// ```
    pub fn split<'a, F>(&'a self, predicate: F) -> Split<'a, F> where F: FnMut(&Harf) -> bool {
        use std::usize;
        Split{iter:self.as_harfs().splitn(usize::MAX, predicate)}
    }
    /// Similar to `ahruf::split` with the exception that no more then `max` 
    /// splits are returned with the last slice containing the rest of the 
    /// original slice.
    ///
    /// 
    /// ```
    ///     use ahruf::{AhrufBuf, Ahruf};
    ///
    ///     let primary = AhrufBuf::from("بيضة\nدجاجة\n\nبيضة");
    ///     let si: Vec<&Ahruf> = primary.splitn(2, |h| h.base() == '\n')
    ///                                  .collect();
    ///
    ///     assert!(si[0] == "بيضة");
    ///     assert!(si[1] == "دجاجة\n\nبيضة");
    ///     assert!(si.len() == 2);
    /// ```
    pub fn splitn<'a, F>(&'a self, max: usize, predicate: F) -> Split<'a, F> where F: FnMut(&Harf) -> bool {
        Split{iter:self.as_harfs().splitn(max, predicate)}
    }
        
    
    /// Segments an Ahruf slice into chunks accoring to `classifier`.
    /// 
    /// For each group of adjecent Harfs that share the same return value 
    /// `Some(C)` as returned by the `classifier` function will be grouped 
    /// together and returned by `Hunks::next` as an Ahruf slice.
    /// 
    /// If the classification function returns `None`, **the preceeding group of 
    /// Harfs will be discarded.**
    ///
    /// ```
    ///     use ahruf::{AhrufBuf, WS};
    ///
    ///     let buffer = AhrufBuf::from("دجاج    أجاج");
    ///     let classified: Vec<_> = buffer
    ///             .hunks(|c| if c.any(WS) {Some("فراغ")} else {Some("كتب")})
    ///             .collect();
    ///
    ///     assert!(classified[0].1 == "دجاج");
    ///     assert!(classified[1].0 == "فراغ");
    ///     assert!(classified[2].1 == "أجاج");
    ///
    /// ```
    ///
    /// Another slightly envolved example ilistrates discarding all white space
    /// and only yeilding non-white-space regions:
    ///
    /// ```
    ///     use ahruf::{AhrufBuf, WS};
    ///
    ///     let ral = "    قد    يمتد    عمر     بعض     أنوع      الدجاج     إلى     ١٠      أعوام       فأكثر    ";
    ///     let primary = AhrufBuf::from(ral);
    ///     
    ///     let classified: Vec<_> = primary
    ///             // Returns an iterator with homoginious regions classified
    ///             .hunks(|c| if c.any(WS) {Some(false)} else {Some(true)})
    ///             // Filters whitespace slices and keeps other slices
    ///             .filter_map(|c| if c.0 {Some(c.1)} else {None})
    ///             // Collects the slices into a vector
    ///             .collect();
    ///
    ///     assert!(classified[0] == "قد");
    ///     assert!(classified[1] == "يمتد");
    /// ```
    pub fn hunks<'a, F, C>(&'a self, classifier: F) -> Hunks<'a, F, C> where F: Fn(&Harf) -> Option<C>, C: PartialEq + Copy {
        Hunks {
            sr: self,
            classifier: classifier,
            cur: 0
        }
    }
    
    
    /// Devides the string into two, with the second slice starting with 
    /// the element at `mid`.
    /// If mid is larger than `self`, the first slice will be the original 
    /// slice abd the second slice will be an empty slice.
    ///
    /// 
    /// ```
    ///     use ahruf::AhrufBuf;
    ///
    ///     let primary = AhrufBuf::from("دجاجة");
    ///     let (head, tail) = primary.split_at(2);
    ///
    ///     assert!(head == "دج");
    ///     assert!(tail == "اجة");
    ///
    /// ```
    pub fn split_at<'a>(&'a self, mid: usize) -> (&'a Ahruf, &'a Ahruf) {
        if mid >= self.len() {
            (self, harfs_as_ahruf(EMPTY_HARFS))
        }
        else {
            let (a, b) = self.as_harfs().split_at(mid);
            (harfs_as_ahruf(a), harfs_as_ahruf(b))
        }
    }
    
    
    /// This function parses the slice as T.
    /// 
    ///
    /// ```
    ///     use ahruf::AhrufBuf;
    ///     
    ///     assert!(AhrufBuf::from("٢٠").parse::<u8>().unwrap() == 20);
    ///     assert!(AhrufBuf::from("٢٠ يوم").parse::<u8>().is_err());
    /// ```
    pub fn parse<T>(&self) -> Result<T, T::Err> where T: FromAhruf{
        T::from_ahruf(self)
    }
    
    
    /// This function returns true only if every Harf comprising the slice 
    /// has *any* of the attributes `s`. 
    ///
    /// ```
    ///     use ahruf::{AhrufBuf, NORM, ALOOSE};
    ///     
    ///     assert!(AhrufBuf::from("دجاجةٌ").assert_attr(NORM) == true);
    ///     assert!(AhrufBuf::from("فترة حضانة بيض الدجاج هي ٢١ يوما.")
    ///             .assert_attr(NORM) == false);
    ///     assert!(AhrufBuf::from("فترة حضانة بيض الدجاج هي ٢١ يوما.")
    ///             .assert_attr(ALOOSE) == true);
    ///     assert!(AhrufBuf::from("فترة حضانة بيض الدجاج هي 21 يوما.")
    ///             .assert_attr(ALOOSE) == true);
    ///     
    ///
    /// ```  
    pub fn assert_attr(&self, s: Attr) -> bool                                       {
        !self.as_harfs().iter().any(|h| !h.any(s))
    }
    
    /// Returns a subslice starting from the end. If `offset`
    /// is larger than the length of the slice, the slice will be returned in
    /// its entirety.
    pub fn suffix(&self, offset: usize) -> &Ahruf {
        if offset < self.len() {
            &self[self.len()-offset..]
        }
        else {
            self
        }
    }
    
    /// Returns a subslice starting from the begining. If `offset`
    /// is larger than the length of the slice, the slice will be returned in
    /// its entirety.
    pub fn prefix(&self, offset: usize) -> &Ahruf {
        if offset < self.len() {
            &self[..offset]
        }
        else {
            self
        }
    }
    
    /// Returns true if the slice contains the specified sub-Ahruf.
    /// 
    /// ```
    ///     use ahruf::{AhrufBuf};
    ///
    ///     let example = AhrufBuf::from("دجاجة");
    ///     assert!(example.contains(&AhrufBuf::from("جة")) == true); 
    ///
    /// ```
    ///
    /// *See also: `AhrufFlow::contains`*
    pub fn contains<T: AhrufLike>(&self, substring: &T) -> bool {
        self.index_of(substring).is_some()
    }
    
    /// Returns the index of the first Harf in self that matches `substring` 
    /// using the mask `mask` for both during comparasion.
    /// 
    /// ```
    ///     use ahruf::{AhrufBuf};
    ///
    ///     let example = AhrufBuf::from("المبالغة في مدة طهي صدر الدجاج يجعله لينًا.");
    ///     assert!(example.index_of(&AhrufBuf::from("في")) == Some(9)); 
    ///
    /// ```
    ///
    /// *See also: `AhrufFlow::index_of`*
    pub fn index_of<T: AhrufLike>(&self, substring: &T) -> Option<usize> {
        let substring_len = substring.ahruf_count();
        
        for i in 0..self.len() {
            if substring_len > self[i..].len() { break }
            
            if substring.equivalent_to_ahruf(&self[i..i+substring_len]) {
                return Some(i);
            }
        }
        None
    }
    
    /// Connect or joins an iterator with the string. 
    /// 
    /// ```
    /// use ahruf::AhrufBuf;
    ///
    /// let produce = [
    ///     AhrufBuf::from("اللحم"), 
    ///     AhrufBuf::from("البيض"),
    ///     AhrufBuf::from("الريش"),
    ///     AhrufBuf::from("السماد")
    /// ];
    /// let benifits = AhrufBuf::from(" و").connect(produce.iter().map(|x| &x[..]));
    /// assert!(benifits == "اللحم والبيض والريش والسماد");
    /// ```
    pub fn connect<'a, T: iter::Iterator<Item=U>, U: AsRef<Ahruf>>(&self, words: T) -> AhrufBuf {
        let s = self;
        let mut words = words;
        let mut a = AhrufBuf::new();
        let mut current = words.next();
        loop {
            let next = words.next();
            if current.is_some() && next.is_some() {
                a.push_slice(current.unwrap().as_ref());
                a.push_slice(s);
                current = next;
            }
            else if current.is_some() && next.is_none() {
                a.push_slice(current.unwrap().as_ref());
                break;
            }
            else {
                break;
            }
        }
        a
    }
    
    
    /// Strips the Tashkel from all Harf elements that have them.
    pub fn strip_tashkel(&mut self) {
        for x in self.as_harfs_mut().iter_mut() { x.strip_tashkel() }
    }
    
    
    /// Trims characters off both edges of a given string. 
    ///
    /// Example
    /// -------
    ///
    /// ```
    /// use ahruf::{AhrufBuf, WS};
    /// 
    /// let k = AhrufBuf::from("   الدجاجة سبقت البيضة    ");
    /// let l = k.trim(WS);
    /// assert!(l == "الدجاجة سبقت البيضة");
    /// ```
    pub fn trim(&self, set: Attr) -> &Ahruf {
        self.trim_start(set).trim_end(set)
    }
    
    
    /// Trims characters off the starting edge of a given string. 
    ///
    /// Example
    /// -------
    ///
    /// ```
    /// use ahruf::{AhrufBuf, WS};
    /// 
    /// let k = AhrufBuf::from("   الدجاجة سبقت البيضة    ");
    /// let l = k.trim_start(WS);
    /// // Note that Bidi is causing the string to be displayed incorrectly
    /// assert!(l == "الدجاجة سبقت البيضة    ");
    /// ```
    pub fn trim_start(&self, set: Attr) -> &Ahruf {
        match self.as_harfs().iter().position(|x| !x.any(set)) {
            Some(ofst) => &self[ofst..],
            None=> &self[0..0]
        }
    }

    /// Trims characters off the terminating edge of a given string. 
    ///
    /// Example
    /// -------
    ///
    /// ```
    /// use ahruf::{AhrufBuf, WS};
    /// 
    /// let k = AhrufBuf::from("   الدجاجة سبقت البيضة    ");
    /// let l = k.trim_end(WS);
    /// // Note that Bidi is causing the string to be displayed incorrectly
    /// assert!(l == "   الدجاجة سبقت البيضة");
    /// ```    
    pub fn trim_end(&self, set: Attr) -> &Ahruf {
        match self.as_harfs().iter().rposition(|x| !x.any(set)) {
            Some(ofst) => &self[..ofst+1],
            None=> &self[0..0]
        }
    }
    
    
    
    /// This function yields an iterator that replaces substrings matching 
    /// `needle` with `replacement`. `AhrufsHarfsAdaptor` can be used to 
    /// convert the `Ahruf` iterator into a that of a `Harf`.
    ///
    /// See also: `AhrufBuf::subsitute`
    pub fn replace<'a, T>(&'a self, needle: T, replacement: &'a Ahruf) -> Replace<'a, T> where T: AhrufLike {
        Replace::replace(self, needle, replacement)
    }
}

impl convert::AsRef<Ahruf> for [Harf] {
    fn as_ref(&self) -> &Ahruf {
        harfs_as_ahruf(self)
    }
}

impl convert::AsMut<Ahruf> for [Harf] {
    fn as_mut(&mut self) -> &mut Ahruf {
        harfs_as_mut_ahruf(self)
    }
}

impl convert::AsRef<Ahruf> for Ahruf {
    fn as_ref(&self) -> &Ahruf {
        self
    }
}

impl convert::AsMut<Ahruf> for Ahruf {
    fn as_mut(&mut self) -> &mut Ahruf {
        self
    }
}

/*
impl<T> cmp::PartialEq<T> for Ahruf where T: AhrufLike {
    fn eq(&self, other: &T) -> bool {
        other.equivalet_to_ahruf(self)
    }
}

impl<'a> cmp::Eq for &'a Ahruf {}
*/

impl cmp::PartialEq<Ahruf> for Harf {
    fn eq(&self, other: &Ahruf) -> bool {
        other == self.as_ref()
    }
}

impl cmp::PartialEq<Ahruf> for Ahruf {
    fn eq(&self, other: &Ahruf) -> bool {
        self.as_harfs() == other.as_harfs()
    }
}

impl<'a> cmp::PartialEq<Ahruf> for &'a Ahruf {
    fn eq(&self, other: &Ahruf) -> bool {
        self.as_harfs() == other.as_harfs()
    }
}

impl cmp::PartialEq<AhrufBuf> for Ahruf {
    fn eq(&self, other: &AhrufBuf) -> bool {
        self == &other[..]
    }
}
/*
impl<'a> cmp::PartialEq<&'a AhrufBuf> for Ahruf {
    fn eq(&self, other: &&AhrufBuf) -> bool {
        self == other[..]
    }
}*/
impl<'a> cmp::PartialEq<AhrufBuf> for &'a Ahruf {
    fn eq(&self, other: &AhrufBuf) -> bool {
        *self == &other[..]
    }
}

impl cmp::PartialEq<str> for Ahruf {
    fn eq(&self, other: &str) -> bool {
        use ::flow::Flow;
        self.masked_flow(0) == other.masked_flow(0)
    }
}
impl<'a> cmp::PartialEq<&'a str> for Ahruf {
    fn eq(&self, other: &&str) -> bool {
        use ::flow::Flow;
        self.masked_flow(0) == other.masked_flow(0)
    }
}

impl<'a> cmp::PartialEq<str> for &'a Ahruf {
    fn eq(&self, other: &str) -> bool {
        use ::flow::Flow;
        self.masked_flow(0) == other.masked_flow(0)
    }
}

impl<'a> cmp::PartialEq<String> for &'a Ahruf {
    fn eq(&self, other: &String) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}

impl<'a> cmp::PartialEq<Ahruf> for &'a str {
    fn eq(&self, other: &Ahruf) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}
impl cmp::PartialEq<Ahruf> for String {
    fn eq(&self, other: &Ahruf) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}

impl<'a> cmp::PartialEq<String> for Ahruf {
    fn eq(&self, other: &String) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}

impl cmp::PartialEq<Ahruf> for str {
    fn eq(&self, other: &Ahruf) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}
impl cmp::PartialEq<Ahruf> for char {
    fn eq(&self, other: &Ahruf) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}
impl cmp::PartialEq<char> for Ahruf {
    fn eq(&self, other: &char) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}



// Index Implementaions
impl ops::Index<usize> for Ahruf {
    type Output = Harf;
    fn index<'a>(&'a self, index: usize) -> &'a Self::Output {
        &self.as_harfs()[index]
    }
}
impl ops::Index<ops::Range<usize>> for Ahruf {
    type Output = Ahruf;
    fn index<'a>(&'a self, index: ops::Range<usize>) -> &'a Self::Output {
        harfs_as_ahruf(&self.as_harfs()[index])
    }
}
impl ops::Index<ops::RangeFrom<usize>> for Ahruf {
    type Output = Ahruf;
    fn index<'a>(&'a self, index: ops::RangeFrom<usize>) -> &'a Self::Output {
        harfs_as_ahruf(&self.as_harfs()[index])
    }
}
impl ops::Index<ops::RangeTo<usize>> for Ahruf {
    type Output = Ahruf;
    fn index<'a>(&'a self, index: ops::RangeTo<usize>) -> &'a Self::Output {
        harfs_as_ahruf(&self.as_harfs()[index])
    }
}
impl ops::Index<ops::RangeFull> for Ahruf {
    type Output = Ahruf;
    fn index<'a>(&'a self, index: ops::RangeFull) -> &'a Self::Output {
        harfs_as_ahruf(&self.as_harfs()[index])
    }
}

// Mutable Indexing Implementations
impl ops::IndexMut<usize> for Ahruf {
    fn index_mut<'a>(&'a mut self, index: usize) -> &'a mut Self::Output {
        &mut self.as_harfs_mut()[index]
    }
}
impl ops::IndexMut<ops::Range<usize>> for Ahruf {
    fn index_mut<'a>(&'a mut self, index: ops::Range<usize>) -> &'a mut Self::Output {
        harfs_as_mut_ahruf(&mut self.as_harfs_mut()[index])
    }
}
impl ops::IndexMut<ops::RangeFrom<usize>> for Ahruf {
    fn index_mut<'a>(&'a mut self, index: ops::RangeFrom<usize>) -> &'a mut Self::Output {
        harfs_as_mut_ahruf(&mut self.as_harfs_mut()[index])
    }
}
impl ops::IndexMut<ops::RangeTo<usize>> for Ahruf {
    fn index_mut<'a>(&'a mut self, index: ops::RangeTo<usize>) -> &'a mut Self::Output {
        harfs_as_mut_ahruf(&mut self.as_harfs_mut()[index])
    }
}
impl ops::IndexMut<ops::RangeFull> for Ahruf {
    fn index_mut<'a>(&'a mut self, index: ops::RangeFull) -> &'a mut Self::Output {
        harfs_as_mut_ahruf(&mut self.as_harfs_mut()[index])
    }
}

impl fmt::Display for Ahruf {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        let mut out = String::new();
        // stupid, but dealing with finiky rust is not a priority 
        for c in self.as_harfs().iter() {
            for i in c.to_string().chars() {
                out.push(i);
            }
        }
        write!(f, "{}", out)
    }
}

impl fmt::Debug for Ahruf {
    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "a\"{}\"", self)
    }
}




#[test]
fn strip_tashkel_test() {
    let mut x = AhrufBuf::from("سَجَعٌ");
    x.strip_tashkel();
    assert!(x == AhrufBuf::from("سجع"));
    let mut x = AhrufBuf::from("Hello, Ben.");
    x.strip_tashkel();
    assert!(x == AhrufBuf::from("Hello, Ben."));
}
#[test]
fn compare_test() {
    use ::defs::{TASH, NUM};
    use ::flow::Flow;
    
    assert!(AhrufBuf::from("سُؤالًا").masked_flow(TASH) == AhrufBuf::from("سؤالا"));
    assert!(AhrufBuf::from("سُؤالًا").masked_flow(TASH) == AhrufBuf::from("سِؤالا").masked_flow(TASH));
    assert!(AhrufBuf::from("1435").masked_flow(NUM) == AhrufBuf::from("١٤٣٥"));
    assert_eq!(AhrufBuf::from("abcd").prefix(2), AhrufBuf::from("ab"));
    assert!(AhrufBuf::from("سَنبل").prefix(2).masked_flow(TASH) == AhrufBuf::from("سُن").masked_flow(TASH));
    assert!(AhrufBuf::from("سَنبل").suffix(2) == AhrufBuf::from("بل"));
    assert!(AhrufBuf::from("بل").suffix(2) == AhrufBuf::from("بل"));
    assert!(AhrufBuf::from("بل").suffix(2) != AhrufBuf::from("با"));
    
}
#[test]
fn connect_test() {
    let list = [
        AhrufBuf::from("rabbits"),
        AhrufBuf::from("bunnies"),
        AhrufBuf::from("hares")
    ];
    assert!(AhrufBuf::from("rabbits, bunnies, hares") == AhrufBuf::from(", ").connect(list.iter().map(|x| &x[..])));
    assert!(AhrufBuf::from("rabbits") == AhrufBuf::from(", ").connect(list[..1].iter().map(|x| &x[..])));
    assert!(AhrufBuf::from("") == AhrufBuf::from(", ").connect(list[..0].iter().map(|x| &x[..])));

}
#[test]
fn contains_harf_test() {
    assert!(!AhrufBuf::from("بلابل").contains(&'س'));
    assert!(AhrufBuf::from("بلابل").contains(&'ب'));
    assert!(!AhrufBuf::from("بلاًبل").contains(&'ا')); // because of tashkel
} 
#[test]
fn pactest() {
    fn any(s: &Ahruf, f: u32) -> bool {s.assert_attr(f)}
    use ::defs::{ALOOSE, NUM, NORM};
    assert!(any(AhrufBuf::from("سَجَعٌ").as_ref(), NORM));
    assert!(any(AhrufBuf::from("سًََََجَعٌ").as_ref(), NORM)); // THIS WILL FAIL IF STRICT HARAKAT CHECKS ARE IN PLACE
    assert!(!any(AhrufBuf::from("سجع وطباق").as_ref(), NORM));
    assert!(!any(AhrufBuf::from("NoجستNo").as_ref(), NORM));
    assert!(any(AhrufBuf::from("إذهب! فالمكان لم يعد يسعك. ترفض؟! أنى لك أن ترفض، وأنا صاحب هذا البيت؟!").as_ref(), ALOOSE));
    assert!(any(AhrufBuf::from("إذا قلنا ان نهاية المتتالية (1\\٢*ن) = صفر أو ١، فماذا ستقول؟").as_ref(), ALOOSE));
    assert!(any(AhrufBuf::from("?*؟").as_ref(), ALOOSE));
    assert!(!any(AhrufBuf::from("No").as_ref(), ALOOSE));
    assert!(any(AhrufBuf::from("١1١1١1١1١1").as_ref(), NUM));
}
#[test]
fn replace_test(){
    use ::{CALEF,TASH};
    let needle = AhrufBuf::from("صوم");
    
    assert!(AhrufBuf::from("صوم").replace(&needle, &AhrufBuf::from("ن")).to_ahruf_buf() == /*‎*/"ن");
    assert!(AhrufBuf::from("  صوم ").replace(&needle, &AhrufBuf::from("ن")).to_ahruf_buf() == /*‎*/"  ن ");
    assert!(AhrufBuf::from("صوم").replace(&needle, &AhrufBuf::from("sssssssssssss")).to_ahruf_buf() == /*‎*/"sssssssssssss");
    assert!(AhrufBuf::from("صو").replace(&needle, &AhrufBuf::from("ss")).to_ahruf_buf() == /*‎*/"صو");
    
    let mut needle = AhrufBuf::from("دجاج");
    needle.subsitute(AhrufBuf::from("ج"), &AhrufBuf::from("ن"));
    assert!(needle == "دنان");
    needle.subsitute(AhrufBuf::from(""), &AhrufBuf::from("ج"));
    assert!(needle == "دنان");
    needle.subsitute("ن", &AhrufBuf::from(""));
    assert!(needle == "دا");
    
    assert_eq!("إن الصَوم جيد؟".masked_flow(TASH|CALEF).replace(&"ص", &"ن"), /*d*/ "ان النوم جيد؟");
}
#[test]
fn test_contains_and_replace() {    
    use ::defs::TASH;
    use ::flow::Flow;
    assert!(AhrufBuf::from("بيت الدجاج الوردي").contains(&AhrufBuf::from("دجاج")) == true);
    assert!(AhrufBuf::from("بيت الدَجاج الوردي").contains(&AhrufBuf::from("دجاج")) == false);
    assert!(AhrufBuf::from("بيت الدَجاج الوردي").masked_flow(TASH).contains("دجاج".flow()) == true);
    assert!(AhrufBuf::from("بيت الدَجاج الوردي").masked_flow(TASH).contains("دجاجُ".masked_flow(TASH)) == true);
    // Change to AhrufFlow test TODO assert_eq!(AhrufBuf::from("بيت الدَجاج الورديُ").replace(TASH, AhrufBuf::from("دجاجُ"), AhrufBuf::from("دواجن"))
    // Change to AhrufFlow test TODO          , /*‎*/"بيت الدواجن الورديُ");
            
    assert_eq!(AhrufBuf::from("دُجاج دُجاج دجاج").replace(AhrufBuf::from("دُجاج"), &AhrufBuf::from("hen")).to_ahruf_buf()
            , /*‎*/"hen hen دجاج");
}
#[test]
fn trim_test() {
    use ::defs::{WS, LSYM};
    let spacey = AhrufBuf::from(" (banana)  ");
    let spacey = spacey.trim(WS);
    assert!(spacey.to_string() == "(banana)"); 
    let spacey = spacey.trim(LSYM);
    assert!(spacey.to_string() == "banana"); 
    
    let macey = AhrufBuf::from("   يَتَخَلَلُ هذا النَصُ فَراغاتٌ مُتَعَدِدَةٌ     ");
    let macey = macey.trim_start(WS);
    assert!(macey.to_string() == "يَتَخَلَلُ هذا النَصُ فَراغاتٌ مُتَعَدِدَةٌ     "); // bidi doing its thing
    let macey = macey.trim_end(WS);
    assert!(macey.to_string() == "يَتَخَلَلُ هذا النَصُ فَراغاتٌ مُتَعَدِدَةٌ");
}
#[test]
fn hunks_test() {
    use ::defs::*;
    let mut s = AhrufBuf::new();
    
    macro_rules! check {
        ($iter: expr, $expected_id: expr, $expected_string: expr) => ({
            let (id, string) = $iter.next().unwrap();
            assert_eq!(id, $expected_id);
            assert_eq!(string, $expected_string);
        });
        ($iter: expr) => ({{assert!($iter.next().is_none())}})
    }
    macro_rules! classifier {
        () => (|c| { if c.any(WS) {Some(0)} else if c.any(NUM) {None} else {Some(1)}})
    }
    
    {
        s.reuse_from_str("a b c1 ddd eeeee5 ");
        let mut iter = s.hunks(classifier!());
        check!(iter, 1, "a");
        check!(iter, 0, " ");
        check!(iter, 1, "b");
        check!(iter, 0, " ");
        check!(iter, 0, " ");
        check!(iter, 1, "ddd");
        check!(iter, 0, " ");
        check!(iter, 0, " ");
        check!(iter);
    }
    {
        s.reuse_from_str("111 22 1 d 235 ");
        let mut iter = s.hunks(classifier!());
        check!(iter, 0, " ");
        check!(iter, 1, "d");
        check!(iter, 0, " ");
        check!(iter);
    }
    {
        s.reuse_from_str("11221d 235");
        let mut iter = s.hunks(classifier!());
        check!(iter, 1, "d");
        check!(iter);
    }
    {
        s.reuse_from_str("abcdef");
        let mut iter = s.hunks(classifier!());
        check!(iter, 1, "abcdef");
        check!(iter);
    }
    {
        s.reuse_from_str("");
        let mut iter = s.hunks(classifier!());
        check!(iter);
    }
    {
        s.reuse_from_str("1 ");
        let mut iter = s.hunks(classifier!());
        check!(iter, 0, " ");
        check!(iter);
    }
    {
        s.reuse_from_str("11");
        let mut iter = s.hunks(classifier!());
        check!(iter);
    }
    // Without explicit scoping
    assert!(AhrufBuf::from("a   bb  cccc").hunks(classifier!()).count() == 5);
    assert!(AhrufBuf::from("111111").hunks(classifier!()).count() == 0);
    
    for _ in AhrufBuf::from("دجاج طازج").hunks(classifier!()) {}
    for _ in AhrufBuf::from("").hunks(classifier!()) {}
    
}

