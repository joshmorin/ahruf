
/// A bit field to store the qualitive properties associated with Harf 
/// bases.
///
/// See the arrtibutes section of the Specification for an exact definition
/// of what characters are included under a given flag.
pub type Attr=u32;




/// Arabic punctuation marks, and some other symbols that appear in the 
/// U+600 block
pub const ASYM:Attr         = 0x0000_0001;

/// Arabic Harfs that have this property are the Harfs this library is 
/// designed to work with. 
/// 
/// Precomposed characters (such as U+FEF7 'ﻷ') and combining character 
/// (Tashkel charactars) and others are not normative. These characters 
/// are converted to their normative counterparts or are attached to their 
/// bases when utf8 is converted to unicode if posible.
///
/// The set of Arabic Letters that are considered normitive is precisly 
/// defined in the attributes section of the specification.
pub const NORM:Attr        = 0x0000_0002;

/// Arabic Numerals
pub const ANUM:Attr         = 0x0000_0004;

/// Arabic Presentation
pub const PRES:Attr        = 0x0000_0008;

/// Tashkel
pub const TASH:Attr        = 0x0000_0010;

/// Arabic Letters and Symbols that aren't treated as Arabic
pub const AUHDL:Attr        = 0x0000_0020;

/// Basic Latin aplhebet letters
pub const LAT:Attr         = 0x0000_0040;

/// Basic Latin symbols
pub const LSYM:Attr         = 0x0000_0080;

/// Basic Latin numerals
pub const LNUM:Attr         = 0x0000_0100;

/// Direction overide Markers, Shaping Markers, ...
pub const FORM:Attr          = 0x0000_0200;   // Change to ARA? Any char in U+600/FE70 would have this
                                                    // Add ASCII for <= 0x7F

/// A set of white space characters that include newlines and unicode 
/// white space characters
pub const WS:Attr            = 0x0000_0400;

/// Letters that can be classified as Alef
pub const CALEF:Attr       = 0x0000_0800;

/// Letters that have the script form of Waw
pub const CWAW:Attr        = 0x0000_1000;

/// Letters that have the script form of Yeh
pub const CYEH:Attr        = 0x0000_2000;

/// Letters that can be classified as hamza
pub const CHAMZA:Attr      = 0x0000_4000;

/// Letters that have symantic-equivilance to Teh
pub const CTEH:Attr        = 0x0000_8000;

/// Characters that are classified as meds
pub const CMED:Attr        = 0x0001_0000;

/// Characters that are attributes to what they follow
pub const AFRAG:Attr        = 0x0002_0000;

/// Characters that are completely ignored by string normalizers
pub const IGN:Attr           = 0x0004_0000;



// -----------------------------------------------------------------------------
/// Sukun Tashkel Flag
pub const TSUKUN:Attr       = 0x0100_0000;

/// Shada Tashkel Flag
pub const TSHADDA:Attr      = 0x0200_0000;

/// Tanween Tashkel Flag
pub const TTANWEEN:Attr     = 0x0300_0000;

/// Tashkel Block (fourth byte; bits 0 and 1)
///
/// ```no_test
///     00: none
///     01: sukun
///     10: shadda
///     11: tanween
/// ```
pub const TSST:Attr        = 0x0300_0000;


// -----------------------------------------------------------------------------
/// Small alef Tashkel Flag
pub const TALEF:Attr        = 0x0400_0000;

/// Small Waw Tashkel Flag
pub const TWAW:Attr         = 0x0800_0000;

/// Small Yeh Tashkel Flag
pub const TYEH:Attr         = 0x0c00_0000;

/// Small med Block (fourth byte; bits 2 and 3;)
///
/// ```no_test
///     00: none
///     01: small alef
///     10: small waw
///     11: small yeh
/// ```
pub const TAWY:Attr        = 0x0c00_0000;


// -----------------------------------------------------------------------------
/// Fatha Tashkel
pub const TFATHA:Attr       = 0x1000_0000;

/// Damma Tashkel Flag
pub const TDAMMA:Attr       = 0x2000_0000;

/// Kasra Tashkel Flag
pub const TKASRA:Attr       = 0x3000_0000;

/// Haraka Block (fourth byte; bits 4 and 5)
///
/// ```no_test
///     00: none
///     01: fatha
///     10: damma
///     11: kasra
/// ```
pub const TFDK:Attr        = 0x3000_0000;


// -----------------------------------------------------------------------------
/// A constant with all Tashkel flags set
pub const TALL:Attr         = TFDK|TAWY|TSST;
// -----------------------------------------------------------------------------

// -----------------------------------------------------------------------------
/// All user configurable atttributes
pub const USER_ATTR:Attr      = TALL;
// -----------------------------------------------------------------------------


/// A superset of all numbers recognized by this library
pub const NUM:Attr           = ANUM|LNUM;

/// A superset of all symbols recognized by this library
pub const SYM:Attr           = LSYM|ASYM;

/// A superset of all sets that usually occure in Arabic-only texts (WS, SYM, NORM, NUM, FORM, and TASH)
pub const ALOOSE:Attr       = WS|SYM|NORM|NUM|FORM|TASH;

