use ::ahruf::Ahruf;
use ::ahrufbuf::AhrufBuf;
use ::harf::Harf;
use ::flow::{AhrufFlow, Flow};
    

/// Anstraction over string types.
///
/// DO NOT MAKE PUBLIC: This is an opaque trait and should be regarded as a Marker 
/// trait by the user.
/// 
/// Dificiancies:
///
/// 1. ahruf_count can be iniffeciant, cache resulting length if posible.
///
pub trait AhrufLike { 
    /// Returns the number of Harfs `Self` represents. Performance will 
    /// vary depending on the underlying representation of the Object.
    fn ahruf_count(&self) -> usize;
    
    /// Tests whether `Self` is equivalent to the iterator `f`
    fn equivalent_to_flow<T: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<T>) -> bool;
    
    /// Tests whether `Self` is equivalent to the string `s`
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool;
    
    /*
    fn index_of_flow<T: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<T>) -> Option<usize>;
    */
    
    /// Returns a refrence to the implementing type
    fn to_ahruf_buf(&self) -> AhrufBuf;
    
    /// Returns the underlying Ahruf slice if posible.
    ///
    /// Maybe change to an enum that either returns a slice or allocates a new 
    /// buffer.
    fn try_ahruf(&self) -> Option<&Ahruf> {None}
}

impl<T> AhrufLike for AhrufFlow<T> where T: Iterator<Item=Harf> + Clone { 
    fn ahruf_count(&self) -> usize {self.clone().count()} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.clone().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        self.clone().ceq(s.flow())
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {self.clone().collect()}
}

impl<'a, T> AhrufLike for &'a AhrufFlow<T> where T: Iterator<Item=Harf> + Clone { 
    fn ahruf_count(&self) -> usize {(*self).ahruf_count()} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        (*self).clone().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        (*self).clone().ceq(s.flow())
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {(*self).clone().collect()}
}

impl AhrufLike for AhrufBuf { 
    fn ahruf_count(&self) -> usize {self.len()}
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        self.as_harfs() == s.as_harfs()
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {self.clone()}
    fn try_ahruf(&self) -> Option<&Ahruf> {Some(self.as_ref())}
}

impl<'a> AhrufLike for &'a AhrufBuf { 
    fn ahruf_count(&self) -> usize {self.len()} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        self.as_harfs() == s.as_harfs()
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {AhrufBuf::clone(self)}
    fn try_ahruf(&self) -> Option<&Ahruf> {Some(self.as_ref())}
}

impl AhrufLike for Ahruf { 
    fn ahruf_count(&self) -> usize {self.len()} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        s.as_harfs() == self.as_harfs()
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {AhrufBuf::from(self)}
    fn try_ahruf(&self) -> Option<&Ahruf> {Some(self)}
}


impl<'a> AhrufLike for &'a Ahruf { 
    fn ahruf_count(&self) -> usize {self.len()} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        s.as_harfs() == self.as_harfs()
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {AhrufBuf::from(*self)}
    fn try_ahruf(&self) -> Option<&Ahruf> {Some(*self)}
}
impl AhrufLike for Harf { 
    fn ahruf_count(&self) -> usize {1} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        s.as_harfs() == self.as_ref().as_harfs()
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {AhrufBuf::from(*self)}
    fn try_ahruf(&self) -> Option<&Ahruf> {Some(self.as_ref())}
}


impl AhrufLike for String { 
    fn ahruf_count(&self) -> usize {<String as AsRef<str>>::as_ref(self).ahruf_count()} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        self.flow().ceq(s.flow())
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {
        use ::flow::Flow;
        self.flow().collect()
    }
}

impl<'a> AhrufLike for &'a str { 
    fn ahruf_count(&self) -> usize {use ::flow::Flow; self.flow().count()} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        self.flow().ceq(s.flow())
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {
        use ::flow::Flow;
        self.flow().collect()
    }
}
impl AhrufLike for str { 
    fn ahruf_count(&self) -> usize {(&self).ahruf_count()} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        self.flow().ceq(s.flow())
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {
        use ::flow::Flow;
        self.flow().collect()
    }
}
impl AhrufLike for char { 
    fn ahruf_count(&self) -> usize {1} 
    fn equivalent_to_flow<U: Iterator<Item=Harf> + Clone>(&self, f: AhrufFlow<U>) -> bool {
        self.flow().ceq(f)
    }
    fn equivalent_to_ahruf(&self, s: &Ahruf) -> bool {
        self.flow().ceq(s.flow())
    }
    fn to_ahruf_buf(&self) -> AhrufBuf {
        AhrufBuf::from(*self)
    }
}

