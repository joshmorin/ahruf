//! Project Terminology and other definitions.
//!
//!
//! 1.  **Character**:      A 32-bit unsigned integer that reffers to a specific 
//!                         element of text as defined by the Unicode Consortium. 
//!                         This term reffers to the `char` built-in variable type 
//!                         when used in the context of the rust language and the 
//!                         built-in `uint32_t` type when used in the context of 
//!                         the C programming language.
//!
//! 2.  **Normative Arabic Characters**: A set of Arabic Characters that are 
//!                         desgnated as the base arabic letter set. A character
//!                         belonging to this set accepts Tashkel.
//!
//! 3.  **Harf**:           A data structure that is capable of fully representing 
//!                         a single Arabic letter along with its attributes, or a 
//!                         single non-Arabic Character. This term reffers to the 
//!                         `Harf` library type when used in the context of the 
//!                         Rust and C programming languages.
//!
//! 4.  **Harf Base**:      The Character a Harf represents without its 
//!                         associated attributes.
//!
//! 5.  **Harf Attributes**: A group of qualities that are either inhearent to the
//!                         the arabic Character itself, or are associated with how 
//!                         a Character is used.
//!
//! 6.  **Harfs**:          An array of Harf objects.
//!
//! 7.  **AhrufBuf**:       A dynamic buffer that holds an array of Harf objects.
//!
//! 8.  **Ahruf Slice**:    A refrence to a subsection of an AhrufBuf or 
//!                         Harfs object.
//!
//! 9.  **Tashkel**:        A group of one or more Arabic diacritic.
//!

