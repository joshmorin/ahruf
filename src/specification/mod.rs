//! This module is documentation only and contains rfcs to which this 
//! library is built, and also contains conformance tests to assert 
//! comformace.
//!
//! To run these tests, pass `--feature "spec-check"` to cargo when running
//! standard testing. Note that these tests are exostive and might take a 
//! long time to complete.
//! 
//! 
//! Because this library fundemntally diverges from the structure unicode 
//! defines for encoded characters, it is necessary to document the 
//! nuances of of this library.
//!
//! Unless stated otherwise, only the behviours and definitions defined 
//! here have any stablitiy gaurentees. Any API not covered herein is 
//! subject to change.


pub mod glossary;
pub mod attributes;
pub mod normalizations;
pub mod harf;
pub mod ahruf;
