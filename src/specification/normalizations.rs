//! Library Conversions and Normalizations.
//! 
//! Certan library functions will normalize strings to preform a computation. 
//! This library will also convert utf-8 strings before processing.
//!
//! This page describes all normalizations and conversions that that this 
//! library implements. If divarging behaviour occurs in the library, please
//! report it as a bug.
//!
//!
//! A. Unicode to Ahruf Conversions
//! --------------------------------
//!
//! 1.  When converting Unicode strings to Ahruf strings, any character 
//!     catagorized    as    `IGN`    will    be    ignored    (see 
//!     `ahruf::specification::attributes`). This includes Tatweel and 
//!     tail-fragment characters, as well as all BIDI formatting characters.  
//! 
//! 2.  Tashkel characters are appended to the letter that perceeds them if 
//!     the letter is defined to have the attribute `NORM`. Otherwise, Tashkel
//!     characters are ignored; Tashkel do not exist as their own Harf in Ahruf
//!     strings returned by unicode conversion functions in this library.
//! 
//! 3.  Composite Arabic charactars (such as the single charater 'ﻻ') are always 
//!     decomposed.
//!
//! 4.  Presentation forms from block U+fe70 normalized to their equivilants in
//!     block U+600.
//!
//! 5.  "ARABIC MADDAH ABOVE" (U+653) is ignored if not perceeded by
//!     the Harf 'ا' (base U+627). If not ignored, the perceeding Harf is 
//!     replaced with 'آ' (base U+622).
//!
//! 6.  "ARABIC HAMZA ABOVE" (U+654) is ignored if not perceeded by the
//!     Harfs 'ا' (base U+627), 'و' (base U+648), or 'ى' (base U+649).
//!     If not ignored, the perceeding Harf is replaced with 'أ' 
//!     (base U+623), 'ؤ' (base U+624), or 'ئ' (base U+626), respectivly.
//!
//! 7.  "ARABIC HAMZA BELOW" (U+655) is ignored if not perceeded by the 
//!     Harf 'ا' (base U+627). If not ignored, the perceeding Harf is replaced
//!     with 'إ' (base U+625).
//!
//!
//! B. Ahruf Masking
//! ----------------
//! 
//! When masking Ahruf strings, characters will be normalized accoriding to the 
//! following:
//!
//! 0. Harfs that are defined to have the AFRAG or IGN attributes are descarded.
//!
//! 1. If the type of normalizations is not explicitly specified, no 
//!    normalization is assumed.
//!
//! 2. A TASH bitwise mask implies that Tashkel information should be striped.
//!
//! 3. A NUM bitwise mask implies that mumerals should be normalized to their U+66X
//!    equivilants.
//!
//! 4. An CALEF mask implies that Harfs classified as CALEF be be normalized to 
//!    Harf 'ا' (base U+627).
//! 
//! 5. An CHAMZA mask implies that Harfs classified as CHAMZA be be normalized to 
//!    Harf 'ء' (base U+621).
//!
//! 6. An CTEH mask implies that Harfs classified as CHAMZA be be normalized to 
//!    Harf 'ت' (base U+62a).
//!
//! Normalization perceednece is currently left for the implementation to define.



#[cfg(feature = "spec-check")]
mod unicode_conv {
    #[test]
    fn ____test____() {
        use ::ahrufbuf::AhrufBuf;
        
        // (A.1)
        assert!(AhrufBuf::from("ـ‎ﹳ").len() == 0);
        // (A.2)
        assert!(AhrufBuf::from("ًٌَُ").len() == 0);
        // Test appending tashkel to a non-norm harf
        let t = AhrufBuf::from("dfُ");
        assert!(t.len() == 2);
        assert!(t[1].has_damma() == false);
        // Test appending tashek to a norm harf
        let t = AhrufBuf::from("مهْ");
        assert!(t.len() == 2);
        assert!(t[1].has_sukun() == true);
        // Test leading stray tashkels
        let t = AhrufBuf::from("ًدجاجٌ");
        assert!(t.len() == 4);
        assert!(t[0] == 'د');
        // (A.3) TODO exostive
        let s = "ﻻ";
        assert!(s.len() == 3);
        let t = AhrufBuf::from(s);
        assert!(t.len() == 2);
        assert!(t[1] == 'ا');
        // (A.4) TODO exostive
        let t = AhrufBuf::from("ﻶ");
        assert!(t.len() == 2);
        assert!(t[1] == 'آ');
        let t = AhrufBuf::from("ﻴ");
        assert!(t.len() == 1);
        assert!(t[0] == 'ي');
        // (A.5) TODO Add composite tests here and make exostive
        let t = AhrufBuf::from("آ");
        assert!(t.len() == 1);
        assert!(t[0] == 'آ');    
        assert_eq!(AhrufBuf::from("ﻻٓ")/*e*/[1], 'آ'); // composite la + MEDDAH ABOVE -> LAM + MED ALEF
        // (A.6) TODO exohstive
        let t = AhrufBuf::from("ىٔ");
        assert!(t.len() == 1);
        assert!(t[0] == 'ئ');
        // (A.7) TODO exohstive
        let t = AhrufBuf::from("إ");
        assert!(t.len() == 1);
        assert!(t[0] == 'إ');
        let t = AhrufBuf::from("وٕ");
        assert!(t.len() == 1);
        assert!(t[0] == 'و');
        
        
        
    }
}

