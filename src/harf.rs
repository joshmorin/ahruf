use ::defs::{   Attr, NORM, ASYM, ANUM, PRES, TASH, 
                AUHDL, LAT, LSYM, LNUM, FORM, AFRAG,
                WS, CALEF, CWAW, CYEH, CHAMZA, CTEH, CMED, 
                IGN, TTANWEEN, TFATHA, TDAMMA, TKASRA, TSUKUN, TSHADDA};

use std::fmt::{Formatter, Error, self};
use std::io::Write;
use std::{convert, ops, cmp, iter};

use ::ahruf::Ahruf;

// Convert a char iterator into a harf iterator
#[derive(Clone)]
pub struct CharsHarfsAdaptor<T: iter::Iterator<Item=char>> {
    iter: T,
    current: Option<Harf>,   
    pending: Option<char>
}
impl<T: iter::Iterator<Item=char>> CharsHarfsAdaptor<T> {
    pub fn new(t: T) -> CharsHarfsAdaptor<T> {
        CharsHarfsAdaptor {
            iter: t,
            current: None,
            pending: None
        }
    }
}
impl<T: iter::Iterator<Item=char>> iter::Iterator for CharsHarfsAdaptor<T> {
    // TODO implement size hint
    type Item=Harf;
    fn next(&mut self) -> Option<Harf> {
        // Note: This function returns in only two locations, the simple_insert
        // macro and the main loop
        macro_rules! simple_insert {
            ($c: expr) => ({
                use ::defs::IGN;
                let h = PrivHarf::from_char($c);
                if !h.any(IGN) {
                    if let Some(input) = self.current {
                        self.current = Some(h);
                        return Some(input);
                    }
                    else {
                        self.current = Some(h);
                    }
                }
            });
            ($c: expr, $d: expr) => ({
                self.pending = Some($d);
                simple_insert!($c);
            })
        }
        macro_rules! tashkel_insert {
            ($c: expr, $t: expr) => ({
                if let Some(mut h) = self.current {
                    h.append_tashkel($t);
                    self.current = Some(h);
                }
                /*else {
                    simple_insert!($c)
                }*/
            })
        }
        macro_rules! hamza_insert {
            ($c: expr, $h: expr) => ({
                if let Some(mut h) = self.current {
                    let last = h.base();
                    self.current = Some(h);
                    match last {
                        /*‎*/'ا' => match $h {
                            Hamza::Above => {
                                PrivHarf::set_base(&mut h, 'أ');
                                self.current = Some(h);
                            },
                            Hamza::Below => {
                                PrivHarf::set_base(&mut h, 'إ');
                                self.current = Some(h);
                            }
                        },
                        /*‎*/'و' => match $h {
                            Hamza::Above => {
                                PrivHarf::set_base(&mut h, 'ؤ');
                                self.current = Some(h);
                            },
                            Hamza::Below => {()}//simple_insert!($c)}
                        },
                        /*‎*/'ى' => match $h {
                            Hamza::Above => {
                                PrivHarf::set_base(&mut h, 'ئ');
                                self.current = Some(h);
                            },
                            Hamza::Below => {()}//simple_insert!($c)}
                        },
                        _ => {()}//simple_insert!($c)}
                    }
                }
                //else {simple_insert!($c)}
            })
        }
        
        macro_rules! med_insert {
            ($c: expr) => ({
                if let Some(mut h) = self.current {
                    if h.base() == 'ا' {
                        PrivHarf::set_base(&mut h, 'آ');
                        self.current = Some(h);
                    }
                    //else {simple_insert!($c)}                    
                }
                //else {simple_insert!($c)}
            })
        }
        
        
        loop {
            // If there's a Harf pending for output
            if let Some(p) = self.pending {
                self.pending = None;
                simple_insert!(p)
            }
            
            let c = self.iter.next();
            
            if c.is_none() {
                if let Some(input) = self.current {
                    self.current = None;
                    return Some(input);
                }
                else {
                    return None;
                }
            }
            let c = c.unwrap();
            
            enum Hamza {
                Above,
                Below
            }
            
            match c {
                // Note that bidi is doing it's thing, going rtl
                /*‎*/ 'ً' => 
                            tashkel_insert!(c, TFATHA|TTANWEEN),
                /*‎*/ 'ٌ' => 
                            tashkel_insert!(c, TDAMMA|TTANWEEN),
                /*‎*/ 'ٍ' =>
                            tashkel_insert!(c, TKASRA|TTANWEEN),
                /*‎*/ 'َ' =>
                            tashkel_insert!(c, TFATHA),
                /*‎*/ 'ُ' => 
                            tashkel_insert!(c, TDAMMA),
                /*‎*/ 'ِ' => 
                            tashkel_insert!(c, TKASRA),
                /*‎*/ 'ّ' => 
                            tashkel_insert!(c, TSHADDA),
                /*‎*/ 'ْ' => 
                            tashkel_insert!(c, TSUKUN),
                /*‎*/ 'ٔ' => 
                            hamza_insert!(c, Hamza::Above),
                /*‎*/ 'ٕ' =>
                            hamza_insert!(c, Hamza::Below),
                /*‎*/ 'ٓ' =>
                            med_insert!(c),
                '\u{FE70}'...'\u{FEFF}' => match c {
                    /*‎*/ 'ﹰ' | 'ﹱ' =>
                                tashkel_insert!(c, TFATHA|TTANWEEN),
                    /*‎*/ 'ﹲ' =>
                                tashkel_insert!(c, TDAMMA|TTANWEEN),
                    /*‎*/ 'ﹴ' =>
                                tashkel_insert!(c, TKASRA|TTANWEEN),
                    /*‎*/ 'ﹶ' | 'ﹷ' =>
                                tashkel_insert!(c, TFATHA),
                    /*‎*/ 'ﹸ' | 'ﹹ' =>
                                tashkel_insert!(c, TDAMMA),
                    /*‎*/ 'ﹺ' | 'ﹻ' =>
                                tashkel_insert!(c, TKASRA),
                    /*‎*/ 'ﹼ' | 'ﹽ' =>
                                tashkel_insert!(c, TSHADDA),
                    /*‎*/ 'ﹾ' | 'ﹿ' =>
                                tashkel_insert!(c, TSUKUN),
                    /*‎*/ 'ﺀ' =>
                                simple_insert!('ء'),
                    /*‎*/ 'ﺁ' | 'ﺂ' =>
                                simple_insert!('آ'),
                    /*‎*/ 'ﺃ' | 'ﺄ' =>
                                simple_insert!('أ'),
                    /*‎*/ 'ﺅ' | 'ﺆ' =>
                                simple_insert!('ؤ'),
                    /*‎*/ 'ﺇ' | 'ﺈ' =>
                                simple_insert!('إ'),
                    /*‎*/ 'ﺉ' | 'ﺊ' | 'ﺋ' | 'ﺌ' =>
                                simple_insert!('ئ'),
                    /*‎*/ 'ﺍ' | 'ﺎ' =>
                                simple_insert!('ا'),
                    /*‎*/ 'ﺏ' | 'ﺐ' | 'ﺑ' | 'ﺒ' =>
                                simple_insert!('ب'),
                    /*‎*/ 'ﺓ' | 'ﺔ' =>
                                simple_insert!('ة'),
                    /*‎*/ 'ﺕ' | 'ﺖ' | 'ﺗ' | 'ﺘ' =>
                                simple_insert!('ت'),
                    /*‎*/ 'ﺙ' | 'ﺚ' | 'ﺛ' | 'ﺜ' =>
                                simple_insert!('ث'),
                    /*‎*/ 'ﺝ' | 'ﺞ' | 'ﺟ' | 'ﺠ' =>
                                simple_insert!('ج'),
                    /*‎*/ 'ﺡ' | 'ﺢ' | 'ﺣ' | 'ﺤ' =>
                                simple_insert!('ح'),
                    /*‎*/ 'ﺥ' | 'ﺦ' | 'ﺧ' | 'ﺨ' =>
                                simple_insert!('خ'),
                    /*‎*/ 'ﺩ' | 'ﺪ' =>
                                simple_insert!('د'),
                    /*‎*/ 'ﺫ' | 'ﺬ' =>
                                simple_insert!('ذ'),
                    /*‎*/ 'ﺭ' | 'ﺮ' =>
                                simple_insert!('ر'),
                    /*‎*/ 'ﺯ' | 'ﺰ' =>
                                simple_insert!('ز'),
                    /*‎*/ 'ﺱ' | 'ﺲ' | 'ﺳ' | 'ﺴ' =>
                                simple_insert!('س'),
                    /*‎*/ 'ﺵ' | 'ﺶ' | 'ﺷ' | 'ﺸ' =>
                                simple_insert!('ش'),
                    /*‎*/ 'ﺹ' | 'ﺺ' | 'ﺻ' | 'ﺼ' =>
                                simple_insert!('ص'),
                    /*‎*/ 'ﺽ' | 'ﺾ' | 'ﺿ' | 'ﻀ' =>
                                simple_insert!('ض'),
                    /*‎*/ 'ﻁ' | 'ﻂ' | 'ﻃ' | 'ﻄ' =>
                                simple_insert!('ط'),
                    /*‎*/ 'ﻅ' | 'ﻆ' | 'ﻇ' | 'ﻈ' =>
                                simple_insert!('ظ'),
                    /*‎*/ 'ﻉ' | 'ﻊ' | 'ﻋ' | 'ﻌ' =>
                                simple_insert!('ع'),
                    /*‎*/ 'ﻍ' | 'ﻎ' | 'ﻏ' | 'ﻐ' =>
                                simple_insert!('غ'),
                    /*‎*/ 'ﻑ' | 'ﻒ' | 'ﻓ' | 'ﻔ' =>
                                simple_insert!('ف'),
                    /*‎*/ 'ﻕ' | 'ﻖ' | 'ﻗ' | 'ﻘ' =>
                                simple_insert!('ق'),
                    /*‎*/ 'ﻙ' | 'ﻚ' | 'ﻛ' | 'ﻜ' =>
                                simple_insert!('ك'),
                    /*‎*/ 'ﻝ' | 'ﻞ' | 'ﻟ' | 'ﻠ' =>
                                simple_insert!('ل'),
                    /*‎*/ 'ﻡ' | 'ﻢ' | 'ﻣ' | 'ﻤ' =>
                                simple_insert!('م'),
                    /*‎*/ 'ﻥ' | 'ﻦ' | 'ﻧ' | 'ﻨ' =>
                                simple_insert!('ن'),
                    /*‎*/ 'ﻩ' | 'ﻪ' | 'ﻫ' | 'ﻬ' =>
                                simple_insert!('ه'),
                    /*‎*/ 'ﻭ' | 'ﻮ' =>
                                simple_insert!('و'),
                    /*‎*/ 'ﻯ' | 'ﻰ' =>
                                simple_insert!('ى'),
                    /*‎*/ 'ﻱ' | 'ﻲ' | 'ﻳ' | 'ﻴ' =>
                                simple_insert!('ي'),
                    /*‎*/ 'ﻵ' | 'ﻶ' =>
                                simple_insert!('ل', 'آ'),
                    /*‎*/ 'ﻷ' | 'ﻸ' =>
                                simple_insert!('ل', 'أ'),
                    /*‎*/ 'ﻹ' | 'ﻺ' =>
                                simple_insert!('ل', 'إ'),
                    /*‎*/ 'ﻻ' | 'ﻼ' =>
                                simple_insert!('ل', 'ا'),
                    _ => simple_insert!(c)
                },
                _ => simple_insert!(c)
            }
        }
    }
}


fn classify(cp: char) -> Attr {
    let c = cp as u32;
    
    if (c >= 0x621 && c <= 0x63A) || (c >= 0x641 && c <= 0x64A) {
        NORM | match cp {
            /*‎*/'ا' => CALEF|CMED,
            /*‎*/'آ' => CALEF|CMED,
            /*‎*/'أ' => CALEF|CHAMZA,
            /*‎*/'إ' => CALEF|CHAMZA,
            /*‎*/'ى' => CALEF|CYEH|CMED,
            /*‎*/'و' => CWAW|CMED,
            /*‎*/'ؤ' => CWAW|CHAMZA,
            /*‎*/'ي' => CYEH|CMED,
            /*‎*/'ئ' => CYEH|CHAMZA,
            /*‎*/'ء' => CHAMZA,
            /*‎*/'ت' => CTEH,
            /*‎*/'ة' => CTEH,
            _ => 0
        }
    }
    else if c >= 0x64B && c <= 0x652 {
       TASH|AFRAG
    }
    else if c == 0x20 {
        WS
    }
    else if c >= 0x660 && c <= 0x669 {
        ANUM
    } 
    else if c >= 0x30 && c <= 0x39 {
        LNUM
    } 
    else if (c >= 0x41 && c <= 0x5A) || (c >= 0x61 && c <= 0x7A) {
        LAT
    }
    else if (c >= 0x21 && c <= 0x2f) || (c >= 0x3A && c <= 0x40) ||
            (c >= 0x5B && c <= 0x60) || (c >= 0x7B && c <= 0x7E) {
        LSYM
    }
    else if c == 0x0609 || c == 0x060A || c == 0x060C || c == 0x060D || 
            c == 0x061B || c == 0x061F || c == 0x066A || c == 0x066B || 
            c == 0x066C || c == 0x066D {
        ASYM
    }
    else if c == 0x0009 || c == 0x000A || c == 0x000B || c == 0x000C || 
            c == 0x000D || c == 0x0020 || c == 0x0085 || c == 0x00A0 || 
            c == 0x1680 || c == 0x2000 || c == 0x2001 || c == 0x2002 || 
            c == 0x2003 || c == 0x2004 || c == 0x2005 || c == 0x2006 || 
            c == 0x2007 || c == 0x2008 || c == 0x2009 || c == 0x200A || 
            c == 0x2028 || c == 0x2029 || c == 0x202F || c == 0x205F || 
            c == 0x3000 || c == 0x180E || c == 0x200B || c == 0x200C || 
            c == 0x200D || c == 0x2060 || c == 0xFEFF {
        WS
    }
    else if c == 0x00AD || c == 0x0600 || c == 0x0601 || c == 0x0602 || 
            c == 0x0603 || c == 0x0604 || c == 0x0605 || c == 0x061C || 
            c == 0x06DD || c == 0x070F || c == 0x180E || c == 0x200B || 
            c == 0x200C || c == 0x200D || c == 0x200E || c == 0x200F || 
            c == 0x202A || c == 0x202B || c == 0x202C || c == 0x202D || 
            c == 0x202E || c == 0x2060 || c == 0x2061 || c == 0x2062 || 
            c == 0x2063 || c == 0x2064 || c == 0x2066 || c == 0x2067 || 
            c == 0x2068 || c == 0x2069 || c == 0x206A || c == 0x206B || 
            c == 0x206C || c == 0x206D || c == 0x206E || c == 0x206F || 
            c == 0xFEFF || c == 0xFFF9 || c == 0xFFFA || c == 0xFFFB {
        FORM|IGN
    }
    else if c == 'ٔ' as u32 || c == 'ٕ'  as u32 {
        CHAMZA|AFRAG
    }
    else if c == 'ٓ'  as u32 {
        CMED|AFRAG
    }
    else if (c >= 0xFE70 && c <= 0xFE72) || c == 0xFE74 || (c >= 0xFE76 && c <= 0xFE7F) {
       TASH|PRES|AFRAG
    }
    else if c >= 0xFE80 && c <= 0xFEFC {
        PRES
    }
    else if c == 'ـ' as u32 || c == 'ﹳ' as u32 {
        IGN
    }
    // a cache-all to tag any arabic chars that fall through
    else if (c >= 0xFE70 && c <= 0xFEFC) || (c >= 0x600 && c <= 0x6FF) {
        AUHDL
    }
    else {0}
}


/// An element of text.
///
/// `Harf` is an object that stores a character and its attributes, and is 
/// anlogus to the `char` type. 
/// 
/// Harf Objects are designed to be cheap to use 
/// and store. The Harf structure is similar in size to a `u64`. It is 
/// as economical to copy and pass as any other machine type.
/// Aditionally, Most Harf methods are also simple enough to be inlined 
/// automaticly by the compiler.
///
/// Each Harf has an associated "Kind" that is determined based on the 
/// character it represents; the base character. `HarfKind` can be one of
/// three kinds:
/// 
/// 1. `HarfKind::ArabicNormative` //TODO filp?
/// 2. `HarfKind::Numeral`
/// 3. `HarfKind::Other`
///
/// Arabic Normative letters are the primary values this library works with, 
/// and are defined to be strictly one of the following characters:
///
/// ```no_test
///     ‭'آ', 'أ', 'ؤ', 'إ', 'ئ', 'ا', 'ب', 'ة', 'ت', 'ث', 'ج', 'ح', 'خ', 
///     ‭'د', 'ذ', 'ر', 'ز', 'س', 'ش', 'ص', 'ض', 'ط', 'ظ', 'ع', 'غ', 
///     ‭'ف', 'ق', 'ك', 'ل', 'م', 'ن', 'ه', 'و', 'ى', 'ي', 'ء'
/// ```
///
/// Normative letters are the only letters that can be "shakeled". These 
/// are also what the library uses to determine whether a string can be 
/// considered arabic or not. (`AhrufBuf::from` does not produce non-normative 
/// arabic letters or tashkel unless the input is malformed)
///
/// When directly converting a character to `Harf`, 
/// however, the resulting Harf will reflect the original character and will
/// not be converted.
///
/// If the base of a Harf is one of the normative letters, it's kind will be 
/// `HarfKind::ArabicNormative`.
/// 
/// `HarfKind::Numeral` is the kind associated with a Harf when its base is
/// either Latin numerals (Arabic) or Arabic numerals (Arabic-indic).
/// 
/// Any other character has the kind `HarfKind::Other`, including any arabic 
/// character that is not in the normative set defined above.
/// 
/// Update: Harfs cannot be constructed directly from a character due to the 
/// fact that a single unicode character can be a partial character or 
/// multiple Ahruf characters or nothing at all (If the character is ignored).
///
/// Converting characters to Harfs can be done indirectly be reflowing the 
/// character and calling next.
///
/// What follows is an ilistration of Harf API usage:
///
/// ```
///     use ahruf::*;
///     
///     let mut h = 'ن'.flow().next().unwrap();
///
///     assert!(h.any(NORM)); 
///     assert!(h.any(LAT) == false);
///     assert!(h.has_shadda() == false);
///
///     h.append_tashkel(TSHADDA);
///     assert!(h.has_shadda() == true);
///     
///
/// ```




/// This should not be exposed to the user! This is only meant for use within
/// the library.
pub struct PrivHarf;
impl PrivHarf {
    pub fn from_char(b: char) -> Harf {
        let mut h = Harf {codepoint: '\0', attributes: 0};
        PrivHarf::set_base(&mut     h, b);
        h
    }

    pub fn set_base(h: &mut Harf, b: char) {
        h.codepoint = b;
        h.attributes = classify(b);
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Hash)]
pub struct Harf {
    codepoint   : char,
    attributes  : Attr,
}
impl Harf {
    /// Partial Attibute Check: At least one flag from `attributes` must be set 
    /// for this function to return true
    pub fn any(&self, attr: Attr) -> bool {
        self.attributes&attr != 0
    }
    
    /// Strict Attibute Check, all flags in `attributes` must be set for this 
    /// function to return true
    pub fn all(&self, attr: Attr) -> bool {
        self.attributes&attr == attr
    }
    pub fn base(&self) -> char {self.codepoint}
    
    pub fn attributes(&self) -> Attr {self.attributes}
    
    pub fn to_integer(&self) -> Option<u32> {
        match self.base() {
            n @ '0'...'9' => Some(n as u32 - '0' as u32),
            n @ '٠'...'٩' => Some(n as u32 - '٠' as u32),
            _             => None
        }
    }
    
    pub fn copy_tashkel(&mut self, other: Harf) {
        self.append_tashkel(other.attributes());
    }
    
    /// Warning: Mixing flags that should not be mixed will leave the tashkel 
    /// of the Harf in an undetermined state. Valid flag combinations are 
    /// defined in the specification.
    pub fn append_tashkel(&mut self, tashkel: Attr) {
        use ::defs::{TALL, NORM};
        if self.any(NORM) {
            self.attributes |= tashkel & TALL;
        }
    }
    /// Sets the Tashkel on a Harf to `tashkel`.
    /// Warning: Mixing flags that should not be mixed will leave the tashkel 
    /// of the Harf in an undetermined state. Valid flag combinations are 
    /// defined in the specification.
    pub fn set_tashkel(&mut self, tashkel: Attr) -> Harf {
        self.strip_tashkel();
        self.append_tashkel(tashkel);
        *self
    }
    /// Strips Taskel from a Harf
    pub fn strip_tashkel(&mut self) {
        use ::defs::TALL;
        self.attributes &= !TALL
    }
    
    pub fn has_sukun(&self)     -> bool {use ::defs::{TSST, TSUKUN};
                                         self.attributes&TSST == TSUKUN}
    pub fn has_shadda(&self)    -> bool {use ::defs::{TSST, TSHADDA};
                                         self.attributes&TSST == TSHADDA}
    pub fn has_tanween(&self)   -> bool {use ::defs::{TSST, TTANWEEN};
                                         self.attributes&TSST == TTANWEEN}
    
    pub fn has_fatha(&self)     -> bool {use ::defs::{TFDK, TFATHA};
                                         self.attributes&TFDK == TFATHA}
    pub fn has_damma(&self)     -> bool {use ::defs::{TFDK, TDAMMA};
                                         self.attributes&TFDK == TDAMMA}
    pub fn has_kasra(&self)     -> bool {use ::defs::{TFDK, TKASRA};
                                         self.attributes&TFDK == TKASRA}
    
    /*
        -- Still don't know if these should be included --
    pub fn has_small_alef(&self)-> bool {use ::defs::{TAWY, TALEF};
                                         self.attributes&TAWY == TALEF}
    pub fn has_small_waw(&self) -> bool {use ::defs::{TAWY, TWAW};
                                         self.attributes&TAWY == TWAW}
    pub fn has_small_yeh(&self) -> bool {use ::defs::{TAWY, TYEH};
                                         self.attributes&TAWY == TYEH}
    */
    
    pub fn has_fathatan(&self)  -> bool {self.has_tanween()&&self.has_fatha()}
    pub fn has_dammatan(&self)  -> bool {self.has_tanween()&&self.has_damma()}
    pub fn has_kasratan(&self)  -> bool {self.has_tanween()&&self.has_kasra()}
    
    pub fn has_haraka(&self)    -> bool {use ::defs::{TALL};
                                         self.attributes&TALL != 0}
    
}
impl fmt::Display for Harf {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        let mut out = String::new();
        out.push(self.base());
        
        if self.has_tanween() {
            if self.has_fatha() {
                out.push('ً')
            }
            else if self.has_damma() {
                out.push('ٌ')
            }
            else if self.has_kasra() {
                out.push('ٍ')
            }
        }
        else {
            if self.has_sukun() {
                out.push('ْ')
            }
            else {
                if self.has_shadda() {
                    out.push('ّ')
                }
                
                if self.has_fatha() {
                    out.push('َ')
                }
                else if self.has_damma() {
                    out.push('ُ')
                }
                else if self.has_kasra() {
                    out.push('ِ')
                }            
            }            
        }
        
        
        write!(f, "{}", out)
    }
    
}


impl fmt::Debug for Harf {
    fn fmt(&self, f: &mut Formatter) -> Result<(), Error> {
        use ::defs::TALL;
        if cfg!(feature = "debug-harf-extended") {
            write!(f, "'{}':{:X}:{:X}:{}", self.base(), self.base() as u32, self.attributes(), if self.any(TALL) {"T"} else {"-"})
        }
        else {
            write!(f, "'{}'", self)
        }
    }
    
}

impl ops::BitOr for Harf {
    type Output = Harf;
    fn bitor(self, rhs: Harf) -> Harf {
        // XXX WTF??! This makes no sense (even if changed to only take attributes into consideration)
        use std::mem::transmute;
        unsafe {transmute::<u64, _>(transmute::<Harf, u64>(self)|transmute::<Harf, u64>(rhs))}
    }
}        

impl<'a> ops::BitOr<&'a Harf> for Harf {
    type Output = Harf;
    fn bitor(self, rhs: &'a Harf) -> Harf {
        self.bitor(*rhs)
    }
}

impl<'a> ops::BitOr<Harf> for &'a Harf {
    type Output = Harf;
    fn bitor(self, rhs: Harf) -> Harf {
        rhs.bitor(*self)
    }
}


impl<'a, 'b> ops::BitOr<&'a Harf> for &'b Harf {
    type Output = Harf;
    fn bitor(self, rhs: &'a Harf) -> Harf {
        (*self).bitor(*rhs)
    }
}

impl cmp::PartialEq<char> for Harf {
    fn eq(&self, other: &char) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}
impl cmp::PartialEq<Harf> for char {
    fn eq(&self, other: &Harf) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}
impl<'a> cmp::PartialEq<char> for &'a Harf {
    fn eq(&self, other: &char) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}
impl<'a> cmp::PartialEq<&'a Harf> for char {
    fn eq(&self, other: &&Harf) -> bool {
        use ::flow::Flow;
        self.flow() == other.flow()
    }
}
impl convert::AsRef<Ahruf> for Harf {
    fn as_ref(&self) -> &Ahruf {
        use ::ahruf::harfs_as_ahruf;
        use std::slice;
        harfs_as_ahruf(unsafe {slice::from_raw_parts(self, 1)})
    }
}


#[test]
fn harf_test_sanity() {
    use ::defs::TFATHA;
    
    assert!(PrivHarf::from_char('b').set_tashkel(TFATHA).has_fatha() == false);
    assert!(PrivHarf::from_char('ث').set_tashkel(TFATHA).has_fatha() == true);
    
    /*
    assert!(PrivHarf::from_char('أ').compare(Crit::Weak, PrivHarf::from_char('ؤ')));
    assert!(!PrivHarf::from_char('ب').compare(Crit::Weak, PrivHarf::from_char('ؤ')));
    assert!(AhrufBuf::from("قٌ‎")[0].compare(Crit::Base, PrivHarf::from_char('ق'))); // note the U+200E after the tanween
    assert!(!AhrufBuf::from("قٌ‎")[0].compare(Crit::Exact, PrivHarf::from_char('ق'))); // note the U+200E after the tanween
    assert!(PrivHarf::from_char('d').compare(Crit::Exact, PrivHarf::from_char('d')));
    */
    assert!(PrivHarf::from_char('0').to_integer() == Some(0));
    assert!(PrivHarf::from_char('9').to_integer() == Some(9));
    assert!(PrivHarf::from_char('٦').to_integer() == Some(6));
    assert!(PrivHarf::from_char('b').to_integer() == None);
    assert!(<AsRef<Ahruf>>::as_ref(&PrivHarf::from_char('O')) == "O");
}


