use ::harf;
use std::{str, iter, slice, option};

macro_rules! iter_type_wrapper_with_lifetime_a {
    ($name: ident, $func: ident, $t: ty) => (
        #[derive(Clone)]
        pub struct $name<'a> ($t);
        impl<'a> Iterator for $name<'a> {
            type Item = harf::Harf;
            fn next(&mut self) -> Option<harf::Harf> {
                self.0.next()
            }
        }
        pub fn $func<'a>(i: $t) -> $name<'a> {
            $name(i)
        }
    );
}
macro_rules! iter_type_wrapper_with_no_lifetime {
    ($name: ident, $func: ident, $t: ty) => (
        #[derive(Clone)]
        pub struct $name ($t);
        impl Iterator for $name {
            type Item = harf::Harf;
            fn next(&mut self) -> Option<harf::Harf> {
                self.0.next()
            }
        }
        pub fn $func(i: $t) -> $name {
            $name(i)
        }
    );
}


iter_type_wrapper_with_lifetime_a!(
    StrFlowIter, 
    str_flow_iter,
    harf::CharsHarfsAdaptor<str::Chars<'a>>
);

iter_type_wrapper_with_lifetime_a!(
    AhrufFlowIter, 
    ahruf_flow_iter,
    iter::Cloned<slice::Iter<'a, harf::Harf>>
);

iter_type_wrapper_with_no_lifetime!(
    CharFlowIter, 
    char_flow_iter,
    harf::CharsHarfsAdaptor<option::IntoIter<char>>
);
iter_type_wrapper_with_no_lifetime!(
    HarfFlowIter, 
    harf_flow_iter,
    option::IntoIter<harf::Harf>
);


    


