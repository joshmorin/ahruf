/*
    Ahruf: A small library for Harf-oriented Arabic text processing.
    Copyright © 2015  Josh Morin <JoshMorin@gmx.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public 
    License along with this program. If not, see <http://www.gnu.org/licenses/>.
*/
//! An Arabic Text Proccessing Library
//! 
//! This library provides a basic string proccessing API for processing Arabic 
//! texts. The primary goal of this project is to provide a solid, effeciant 
//! foundation for other libraries and applications to build upon.
//!
//! As such, the scope of this library has been narowed down to character
//! and basic string manipulation. Phonetics and gramatical processing are 
//! explicitly designated as out-of-scope.
//!
//! Warning
//! -------
//!
//! I **strongly** advised against using this library to process strings 
//! that will be used in contexts that require byte-for-byte corispondence 
//! to the original string, such as information security and data encoding.
//! 
//! To make Arabic text processing as simple and effient as posible, this 
//! library descards two stricltly decorative codepoints that carry no 
//! significance on either the semantic meaning of the text, or the correct 
//! way it is presented. The two characters are the Arabic Tatweel and 
//! Arabic Tail Fragment characters. Furthermore, this library does not store 
//! the order in which Tashkel appears on a Harf. For further details on this matter, 
//! refer to the documentation of `AhrufBuf` and `Harf`. (TODO)
//!
//! Featurs
//! -------
//!
//! 1. First-class support for Iterator-based strings and stack-bound, lazy 
//!    string operations. Dynamic Buffer-backed strings are also supported as 
//!    first-class types.
//!
//!
//! Design and Rationale
//! --------------------
//!
//! In Unicode, Each character has a unique codepoint. Letters, numerals, 
//! punctuation marks, and even combinational marks have an assigned codepoint. 
//! Unicode strings are a sequence of codepoints. Arabic strings 
//! expressed as Unicode sequences will have a codepoint to each letter, 
//! Haraka, and other marks. The string 'مِنَّ' would be represented in Unicode as
//! the sequence (U+0645, U+0650, U+0646, U+0651, U+064E). Note that this 
//! representation will remain semanticly equivilant if the positions of the last two codepoints
//! are interchanged.
//! 
//! Unicode also defines compound codepoints that are eqvilant to one or more 
//! codepoints. The single codepoint 'ﻻ' is semanticly eqvilant to the 
//! two-codepoint sequence ('ل', ‎'ا'), and 'ﺿ' is equvilant to 'ض'.
//! 
//! Unfortunetly, this flexability in encoding information comes with a 
//! complexity and performace panalty. Unicode defines a set of techneques (e.g. NFC 
//! Normaliation) to mitigate these costs. However, these 
//! messures do not nigate the panalty for arabic texts.
//! 
//! In Arabic, a letter may have multiple marks associated with it. These marks 
//! are a property of that instance of the letter. For example, 
//! the second letter of the 'من' example above has two tashkel marks 
//! associated with it. When encoded as unicode, the letter and its Tashkel cannot 
//! be encoded as a single codepoint because unicode
//! does not define such a codepoint. In latin scripts, such sequences would 
//! have an associated codepoint, such as the codepoint 'ú', and thus do not 
//! suffer from the same issue.
//!
//! By having a single character span multiple codepoints, it becomes nessary to
//! start from the begining of the string when indexing a character, and to 
//! search for the characters boundries when reffering to its associated 
//! information. Furthermore, Invalid codepoint sequence issues arrise because
//! marks can be specified in arbitrary orders and instences.
//!
//! To avoid this performace penilty, We have opted to store Arabic characters
//! with their associated information as a single unit, and to store them in 
//! their normitve forms. 
//! By ensuring that characters are normaitive and complete, computational costs 
//! and complexity can be brought down to equate that of other well supported 
//! scripts.
//! 
//! 
//! *Text formatting and direction of Arabic will most likely be affected by 
//! the bidi algorithm implemented in the text-rendering engine used to render
//! this document. Word ordering might not be what it seems.*
//!
//! Examples
//! --------
//!
//! ```
//!     extern crate ahruf;
//!     use ahruf::{AhrufBuf, Flow, TASH};
//!     
//!     fn main() {
//!         let s = AhrufBuf::from("السَلامُ عَلَيكُم");
//!         
//!         // `s` and its rhs do not match because they differ in their tashkel
//!         assert!(s != "السلام عليكم");
//!         
//!         // ... but without tashkel they do!
//!         assert!(s.masked_flow(TASH) == "السلام عليكم");
//!         
//!         // 6th letter is 'م' and not 'ا'; character access cost is O(1)
//!         assert!(s[5].base() == 'م');
//!         
//!         // That's because each letter knows the tahskeling information 
//!         // associated with it. Tashkel is stored as a Harf's attribute.
//!         assert!(s[5].has_damma() == true);
//!     }
//!
//! ```
//!
//! See `ahruf::tutorials` for a more detailed overview.
//! 
//! Copyrights
//! ==========
//! Copyright © 2015  Josh Morin <JoshMorin@gmx.com>
//!
//! This program is free software: you can redistribute it and/or modify
//! it under the terms of the **GNU Lesser General Public License** as 
//! published by the Free Software Foundation, either version 3 of the 
//! License, or (at your option) any later version.
//!
//! This program is distributed in the hope that it will be useful,
//! but WITHOUT ANY WARRANTY; without even the implied warranty of
//! MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//! GNU Lesser General Public License for more details.
//!
//! You should have received a copy of the GNU Lesser General Public 
//! License along with this program. If not, see <http://www.gnu.org/licenses/>.
//! 
//!
// - IMPORTANT:  All unicode data (char/str) must pass through 
//               CharsHarfsAdaptor which will handle converting 
//               unicode strings to the Ahruf String Model
//

// Why ToHarf was removed: Harf is gaurenteed to be allowed, and complete. 
// char's can be ignored and partial or even a composite of multiple Harfs. 
// They are fundemently two different things 

// Tutorials
// - Write in a way to allow C users to follow along (maybe two seperate guides?)
// - quick overview of termenology
// - a word about how the spec is a refrence and this library an 
//   implementation.
// - Basics
//      - what is a Harf
//          - Two Harf with the same base but different tashkel are not equivilant (strip or mask the tashkel if alternate beviour is desired)
//      - What is a Ahruf
//          - AhrufBuf and Ahruf
//          - What is the API distinction between Ahruf anf AhrufBuf? Ahruf owns data and can change in size.
//              all operations that effect length are defined for AhrufBuf only. Both can be muutated.
//      - ergonomics
//          - Library designed to work well with rust's standard library
//          - can always drop down to harfs representation `[Harf]` and make use of Rust's std slice API
//        mutable -> Ahrufand imutable operations
//      - types of strings
//          - byte array and utf8
//          - AhrufBuf
//          - Ahruf Slice
//          - Harf Array (wont be using much but good to know)
//      - Performance
//          - Native format (Ahruf) vs norrmal utf8 strings (plots)
//              - compare and replacing strings
//              - 
//          - When to use one over the other
//          - Reusing strings (expands to the largest string processed no reallocs)
//      - Initializing strings
//          - Compile-type, run-time
//          - characters that are ignored when converting or normalizing a 
//            string
//          - dealing with wanted IGN characters using AhrufFlow
// - Masks, Comparasions, and normalization
// - 
// - Segmentation and iterations

pub mod tutorials;
pub mod specification;


pub mod types;
mod pluming;
mod flow;
mod defs;
mod harf;
mod ahruf;
mod ahrufbuf;
mod parse;


// Public API 
pub use ::ahruf::{
    Ahruf, 
    Split, 
    Hunks,
    Replace,
    harfs_as_ahruf,  // TODO to -> as
    harfs_as_mut_ahruf,
    AhrufsHarfsAdaptor,
};
pub use ::ahrufbuf::{
    AhrufBuf
};/*
pub use ::chunks::{
    ChunkParser,
    ChunkIter
};*/
pub use ::harf::{
    Harf, 
    CharsHarfsAdaptor,
};
pub use ::flow::{
    ReplaceFlow,
    AhrufFlow,
    Flow
};
pub use ::parse::{
    FromAhruf
};
pub use ::pluming::{
    AhrufLike
};
pub use ::defs::*;


/*
/// Returns a slice of a string as a AhrufBuf
/// For now it's a simple wrapper for AhrufBuf::from(), and thus can't be used
/// for compile-time Ahruf strings. This is planned to change once compiler 
/// plugins stablelizes.
///
/// Temporary behaviour: Will panic if str contains invalid codepoint sequences.
/// Temporary return : AhrufBuf
/// Planned behaviour: Compilation error if invalid input.
/// Planned return: &[Harf]
///
#[macro_export]
macro_rules! ahruf {
    ($e: expr) => (match AhrufBuf::from_strict($e) {
        Err(a) | Ok(a) => &a})
}
*/




#[test]
fn test_sanity() {
    use ::flow::Flow;
    
    let c = "Hi. ﻻ تُدَّخِنُ تَدْخينًا ١";
    let s = AhrufBuf::from(c);
    assert_eq!(s[4].base(), 'ل');
    assert!(s[5].base() == 'ا');
    assert!(s[7].base() == 'ت');
    assert!(s[7].has_damma());
    assert!(s[8].base() == 'د');
    assert!(s[8].has_fatha() && s[8].has_shadda());
    assert!(s[9].base() == 'خ');
    assert!(s[9].has_kasra());
    assert!(s[10].base() == 'ن');
    assert!(!s[10].has_kasra());
    assert!(s[s.len()-1].any(NUM));
    assert!(s == &(s.to_string())[..]);
    
    let mut no = AhrufBuf::from("لاُ");
    assert!(&s[4..6] == no.as_ref().masked_flow(TASH));
    no.strip_tashkel();
    assert!(no.to_string() == "لا");
    assert!(&s[4..6].to_string() == "لا");
    
    let a = AhrufBuf::from("123 د");
    let b = AhrufBuf::from("١٢٣ دً");
    assert!(a != b);
    assert!(&a[..3] != &b[..3]);
    assert!(&a[..3].masked_flow(NUM) == &b[..3].masked_flow(NUM));
    assert!(a.suffix(1) == "د");
    assert!(b.suffix(1) == "دً");
    assert!(a.suffix(2).masked_flow(TASH) == b.suffix(2).masked_flow(TASH));
    
    // ignoring IGN characters
    assert!(AhrufBuf::from("ﹳيا") /*g*/ == "يـــــــــــــــا");
    
    let j: AhrufBuf = "اسف جدا".masked_flow(CALEF).collect();
    assert!(j.contains(&AhrufBuf::from("اسف")));
    assert!(j.contains(&j));
}


