/*

    optimization check list:
    memory allocations valgrind
    system calls strace
        
        
*/

extern crate ahruf;
extern crate time;
mod data;

fn main() {
    use ahruf::*;
    use time::get_time;
    use data::TEST_BLOB_1;

    #[derive(PartialEq, Copy, Clone)]
    enum Class {
        Ignore,
        Proccess
    }
    
    let count = 10000;
    let size = (TEST_BLOB_1.len()*count) as f64/(1024.0*1024.0); // in MB
    let mut buffer = AhrufBuf::new();
    
    // -------------------------------------------------------------------------    
    let mut baseline_unused: u64 = 0; 
    let baseline_time = get_time();
    for _ in 0..count {
        for c in TEST_BLOB_1.chars() {
            baseline_unused += (c as u32) as u64;
        }
    }
    let baseline_time = (get_time() - baseline_time).num_milliseconds() as f64 / 1000.0;
    println!("
            total bytes processed: {:.02} MB
            baseline: {:.2} MB/s ({:.02} s){}", 
            size, size/baseline_time, baseline_time, if baseline_unused%2==0 {""} else {" "});
    // -------------------------------------------------------------------------    
    {
        let mut simple_ignore_count: usize = 0;
        let mut simple_proccess_count: usize = 0;
        let simple_time = get_time();
        for _ in 0..count {
            buffer.reuse_from(TEST_BLOB_1);
            let p = buffer.hunks(|c| 
                if c.base() == ' ' {Some(Class::Ignore)}
                else {Some(Class::Proccess)
            });
            for x in p {
                match x {
                    (Class::Ignore, _) => simple_ignore_count+=1,
                    (Class::Proccess, _) => simple_proccess_count+=1
                }
            }
        }
        let simple_time = (get_time() - simple_time).num_milliseconds() as f64 / 1000.0;
        println!("
            [score: {:.2}]
            simple: {:.2} MB/s ({:.02} s)
            ignored segments: {}
            proccessing segments: {}",
            simple_time/baseline_time*100.,
            size/simple_time, simple_time,
            simple_ignore_count, 
            simple_proccess_count);
    }
    
    // -------------------------------------------------------------------------    
    {
        let mut full_ignore_count: usize = 0;
        let mut full_proccess_count: usize = 0;
        let full_time = get_time();
        for _ in 0..count {
            buffer.reuse_from(TEST_BLOB_1);
            let p = buffer.hunks(|c| 
                if c.any(WS|SYM) {Some(Class::Ignore)}
                else {Some(Class::Proccess)}
            );
            for x in p {
                match x {
                    (Class::Ignore, _) => full_ignore_count+=1,
                    (Class::Proccess, _) => full_proccess_count+=1
                }
            }
        }
        let full_time = (get_time() - full_time).num_milliseconds() as f64 / 1000.0;
        println!("
            [score: {:.2}]
            full: {:.2} MB/s ({:.02} s)
            ignored segments: {}
            proccessing segments: {}",
            full_time/baseline_time*100.,
            size/full_time, full_time,
            full_ignore_count, 
            full_proccess_count);
    }
    
    /*
    // -------------------------------------------------------------------------    
    {
        use std::collections::HashMap;
        let mut ignore_count: usize = 0;
        let mut proccess_count: usize = 0;
        let mut map = HashMap::new();
        let mut p = AhrufIter::new(|c| 
            if c.belongs_(WS|SYMBOL)
               {Some(Class::Ignore)}
            else {Some(Class::Proccess)}
        );
        let time = get_time();
        for _ in 0..count {
            p.parse(TEST_BLOB_1);
            while let Some(x) = p.next() {
                match x {
                    (Class::Ignore, _) => ignore_count+=1,
                    (Class::Proccess, s) => {
                        let score = map.get(s).map(|x| *x).unwrap_or(0);
                        map.insert(s.to_ahruf_buf(), score+1);
                        proccess_count+=1
                    }
                }
            }
        }
        let time = (get_time() - time).num_milliseconds() as f64 / 1000.0;
        println!("
            [score: {:.2}]
            {}: {:.2} MB/s ({:.02} s)
            ignored segments: {}
            proccessing segments: {}",
            time/baseline_time*100.,
            "trim",
            size/time, time,
            ignore_count, 
            proccess_count);
        for (k, v) in map.iter() {
            println!("'{}' = {}", k, v);
        }
    }
    */
}
