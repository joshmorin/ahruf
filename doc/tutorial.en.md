// - Basics
//      - what is a Harf
//          - Two Harf with the same base but different tashkel are not equivilant (strip or mask the tashkel if alternate beviour is desired)
//      - What is a Ahruf
//          - AhrufBuf and Ahruf
//          - What is the API distinction between Ahruf anf AhrufBuf? Ahruf owns data and can change in size.
//              all operations that effect length are defined for AhrufBuf only. Both can be muutated.

# Introduction

This document serves as an introduction to the Ahruf Library. It is not 
intended to be a comprihensive refrence and should not be used as such.
For a complete description of library, refer to the API documentation.

The Ahruf Library is a small software library for effeciant, Harf-level text 
processesing with the primary goal of providing a solid foundation for other 
libraries and applications to build upon.


# The Ahruf String Model

The Ahruf Library and the Unicode standard differ in how strings are logicaly 
structured. Strings are composed of text elements called `Harf`s. A Harf
constitutes the fundemental unit of the library. A Harf consists of a Unicode 
character as a `base` and a set of `attributes`. Harf attributes can be 
intrinsic, such as a character belonging to some `catagory`, or 
extrinsic, such as a Harf's `Tashkel`.

Unlike Unicode, Tashkel (Arabic diacritics) is an attibute of a Harf rather 
than a independent unit. 




Under the Unicode model, strings are a sequence of characters that 
may be letters, diacritics, composite characters and so on. The following 
example illestrates the logical structure of a Unicode string:

        "ﻻ تُكذِّبه"
        [لا] [ ] [ت] [◌ُ] [ك] [ذ] [◌ّ] [◌ِ] [ب] [ه]




characters can be 


In Unicode, 


The Ahruf Library diverges from Unicode in how it presents strings. 

`Harf`s are the fundemental unit of the library. They 

# Sample
    df


Despite the logical string model the Ahruf library adopts, the library  designed to work with Unicode strings, and more 
specificly the Arabic code blocks. This document assumes that the reader is
familiare with the Unicode standard and how Arabic is 
Arabic letters are encoded in 
and diacritics are encoded by uslogicly compose and represented in-string is assumed.

This includes the Decomposable


#Background

This section describes how computer systems represent text. If you're 
familar with both Unicode and the UTF-8 encoding, feel free to skip 
this section. 



When handling 
There are two primary aspects that must be descused when describing strings.
The logical 

Descussing strings, one must 

Modern computer systems store strings as a series of integers. 


Text elements are encoded as a string of integers when processed by comuter 
systems. Each destinced text element (also called a *character*) is represented 
by a single integer. Text elements can be anything including letters, diacritics, numerals,
punctuation, computer-specific signals, and even simple graphics (emoji). The text elemenets
that can be represented (i.e have an associated integer) with 



